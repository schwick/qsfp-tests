#!/usr/bin/python3
import os
import subprocess
import shlex

#PRE = "/home/schwick/mnt/rtm-lab40-r02-board05/DTH_Script/TestBench/PRBS_TEST"
PRE = "/home/schwick/mnt/eoshome/CERN/CMS/DAQ/Phase2/DTH/Technical/PRBS_Test"

cmd = 'find ./ -type f -name "*.py"'
cmdlist = shlex.split( cmd )
print( cmdlist )

res = subprocess.run( cmdlist, capture_output=True, text = True )
#print(res.stdout)
src_files = res.stdout.split()

for src in src_files:
    rtmsrc = os.path.join( PRE, src )
    if not os.path.isfile( rtmsrc ):
        print( "%s does not exist on remote." % rtmsrc ) 
        continue

    cmd = 'diff ' + src + ' ' + rtmsrc
    cmdlist = shlex.split( cmd )
    res = subprocess.run( cmdlist, capture_output=True, text=True)
    if res.stdout != "":
        print( src + " differs" )
        print( res.stdout )
