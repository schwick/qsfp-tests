import pandas as pd
import sys
import plotly as plt
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots

infile = sys.argv[1]
outdir = "plots"

df = pd.read_csv( infile, header=0 )

print(df.describe())

# find row with lowest error sum for each v_diff
df['errorsum'] = df['err_0'] + df['err_1'] + df['err_2'] + df['err_3']

ef = df[df.lock == 15]

gf = ef[ef.errorsum < 1]
print("No errors:")
print( gf )
sys.exit()

for i in range(0,32):
    print( "vdiff %d" %i )
    tf = df[ df.diff_v == i ]
    #print( tf )

    ima = []
    for pre in range(0,32):
        r=[]
        for post in range(0,32):
            r = tf[tf.pre == pre]['errorsum']
            #print (r)
        ima.append(r)
    #print(ima)
    fig = px.imshow(ima, text_auto=True, zmin=-4, zmax=20,
                    labels=dict(x='pre cursor', y='post cursor'))
    fig.write_html( outdir + "/out_" + str(i) + ".div", div_id="v_"+str(i), full_html=False, include_plotlyjs="False", default_width='400px', default_height='400px')



fn = outdir + "/out.html"
fd = open( fn, "w" )
fd.write( "<html>\n" )
fd.write( '<head><script charset="utf-8" src="plotly.min.js"></script>\n')
#fd.write( '<style> div { display:inline; } </style> \n')
fd.write( '</head>\n' )
fd.write( '<body>' )
fd.write( '<h1>'+infile[:-4]+'</h1>')    
for i in range(0,32):
    fd.write('<h2> V_diff index: '+str(i)+'</h2>')
    dn = outdir + "/out_" + str(i) + ".div"
    with open(dn) as infile:
        for line in infile:
            fd.write(line)
fd.write( '</body>' )
fd.write( '</html>' )
fd.close()
