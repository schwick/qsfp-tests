#!/usr/bin/python3
import pandas as pd
import numpy as np
import sys
import shutil
import os
import plotly as plt
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from optparse import OptionParser
from glob import glob
from threading import Thread
import math


MAXPLOTS=32
# for testing:
# MAXPLOTS=4

imgtype = "png"

respath = os.getenv('RESOURCEPATH') 

parser = OptionParser()
parser.add_option("-s", "--skip",
                  action="store_false", dest="do_process", default=True,
                  help="don't process the data but just do the plots")
parser.add_option("-b", "--berr",
                  action="store_false", dest="berr", default=True,
                  help="make plots with log berr as z-axis")
parser.add_option( "-f", "--file", 
                  dest="infile", default="_all_",
                  help="Name of the input file. If set to _all_ process all csv files in the current directory.")

(options, args) = parser.parse_args()




################################################################################################
def getAllCSVFiles():

    csvFiles = glob( "*.csv" )
    return csvFiles

def correct_unlocked(df):
    for index,row in df.iterrows():
        if row['lock'] == 15:
            continue        
        lmask = int(row['lock'])        
        if lmask & 0x01 == 0 :
            df.at[index,'err_0'] = -1
        if lmask & 0x02 == 0:
            df.at[index,'err_1'] = -1
        if lmask & 0x04 == 0:
            df.at[index,'err_2'] = -1
        if lmask & 0x08 == 0:
            df.at[index,'err_3'] = -1
    return df
            
def processInfile( infile, berr=True ):

    if berr:
        outdir = "plots_berr_" + infile[:-4] 
    else:
        outdir = "plots_" + infile[:-4]
    
    if options.do_process and os.path.isdir( outdir ):
        print("Directory exists: " + outdir )
        sys.exit()
    elif not os.path.isdir( outdir ):
        os.mkdir( outdir )
    
    shutil.copy(os.path.join( respath, "prbs.js" ), outdir )
    shutil.copy(os.path.join( respath, "prbs.css" ), outdir )
    shutil.copy(os.path.join( respath, "jquery.min.js" ), outdir )
    
    
    if options.do_process:
        df = pd.read_csv( infile, header=0 )

        df = correct_unlocked(df)
        
        
        # find row with lowest error sum for each v_diff
        df['errorsum'] = df['err_0'] + df['err_1'] + df['err_2'] + df['err_3']
        #df['errorsum'].replace(0,1, inplace=True)
        df[['errorsum','err_0','err_1','err_2','err_3']] = df[['errorsum','err_0','err_1','err_2','err_3']].replace(0,1)

        ef = df[df.lock == 15]
        
        gf = ef[ef.errorsum < 1]
        print("No errors:")
        print( gf )
                
        for i in range(0,MAXPLOTS):
            print( "vdiff %2d" %i )
            tf = df[ df.diff_v == i ]
            ima = []
            for pre in range(0,32):
                if berr :
                    r = np.log10( (tf[tf.pre == pre]['errorsum']) / (100.0e9*tf[tf.pre == pre]['delta_t'] ))
                else:
                    r = tf[tf.pre == pre]['errorsum']
                ima.append(r)

            fig = px.imshow(ima, text_auto=True,
                            labels=dict(x='pre cursor', y='post cursor'))
            fig.update_layout(
                margin=dict(l=0, r=0, t=0, b=0)
            )

            #fig.write_html( outdir + "/out_" + str(i) + ".div", div_id="v_"+str(i), full_html=False, include_plotlyjs="False", default_width='400px', default_height='400px')
            fig.write_image( outdir + "/out_" + str(i) + "." + imgtype)
        
        # Now produce plots for individual differential lines
        for il in range(0,4):
            linelabel = "err_"+str(il)
            for i in range(0,MAXPLOTS):
                print( "vdiff %2d  linelabel %d" % (i, il)  )
                tf = df[ df.diff_v == i ]
                ima = []
                for pre in range(0,32):
                    if berr == True:
                        r = np.log10( (tf[tf.pre == pre][linelabel]) / (25.0e9 * tf[tf.pre == pre]['delta_t']))
                    else:
                        r = tf[tf.pre == pre][linelabel]
                    ima.append(r)
                fig = px.imshow(ima, text_auto=True,
                                labels=dict(x='pre cursor', y='post cursor' ))
                fig.update_layout(
                    margin=dict(l=0, r=0, t=0, b=0)
                )

                fig.write_image( outdir + "/line_" + str(il) + "_out_" + str(i) + "." + imgtype)
            
    
    
    #fn = outdir + "/out.html"
    #fd = open( fn, "w" )
    #fd.write( "<html>\n" )
    #fd.write( '<head><script charset="utf-8" src="plotly.min.js"></script>\n')
    #fd.write( '<script charset="utf-8" src="jquery.min.js"></script>\n')
    #fd.write( '<script charset="utf-8" src="prbs.js"></script>\n')
    #fd.write( '<link rel="stylesheet" href="prbs.css">\n')
    #fd.write( '</head>\n' )
    #fd.write( '<body onload="initPlot()">' )
    #
    #plot_div( fd, infile, "line_0_out_", imgtype )
    #plot_div( fd, infile, "line_1_out_", imgtype )
    #plot_div( fd, infile, "line_2_out_", imgtype )
    #plot_div( fd, infile, "line_3_out_", imgtype )
    #
    #plot_div( fd, infile, "out_", imgtype )
    #    
    #
    #
    #fd.write( '</body>' )
    #fd.write( '</html>' )
    #fd.close()


################################################################################################

infilename = options.infile


if infilename == "_all_":
    infiles = getAllCSVFiles()
else:
    infiles = [infilename]

print(infiles)


threads = []
for infile in infiles:

    t = Thread( target=processInfile, args = [infile, options.berr], name=infile )
    t.start()
    threads.append( t )


for t in threads:
    t.join()
    print( "   Thread %s ended." % t.name )
    

print("All threads returned.")

