import sys
import os 
import hal
import time
from I2CInterface import I2CInterface
   
os.system('clear')

dbg = False

dth = hal.AXIDevice("axi_chip2chip@a0000000", "DTHAddressMap.dat")
     
i2c_QSFP = I2CInterface(dth, 0xA0, offsetWidth=0x1, dataWidth=0x1, items={'i2c_a': 'qsfp_i2c_access_a', 'i2c_b': 'qsfp_i2c_access_b', 'i2c_poll': 'qsfp_i2c_access_a_access_done'})
i2c_switch = I2CInterface(dth, 0xE0, offsetWidth=0x0, dataWidth=0x1, items={'i2c_a': 'qsfp_i2c_access_a', 'i2c_b': 'qsfp_i2c_access_b', 'i2c_poll': 'qsfp_i2c_access_a_access_done'})
 
# order of QSFP on TCA component
switch_TCA = [0x01, 0x02, 0x04, 0x08, 0x10]
bit_scale = 0.1  # uW 

#   remove local reset
dth.write("reset_hw_comp", 0x80000000)
time.sleep(0.1)
dth.write("reset_hw_comp", 0x00000000) 

# data_in_rg(0)                         <= QSFP_A_MODSEL_reg;   -- module select
# data_in_rg(1)                         <= QSFP_A_RESETL_reg;   -- module reset
# data_in_rg(2)                         <= QSFP_A_MODPRSL;      -- module present
# data_in_rg(3)                         <= QSFP_A_INTL;         -- interruption
# data_in_rg(4)                         <= QSFP_A_LPMODE_reg;   -- low power mode   

# data_in_rg(8)                         <= QSFP_B_MODSEL_reg;   -- module select
# data_in_rg(9)                         <= QSFP_B_RESETL_reg;   -- module reset
# data_in_rg(10)                        <= QSFP_B_MODPRSL;      -- module present
# data_in_rg(11)                        <= QSFP_B_INTL;         -- interruption
# data_in_rg(12)                        <= QSFP_B_LPMODE_reg;   -- low power mode   

# data_in_rg(16)                        <= QSFP_C_MODSEL_reg;   -- module select
# data_in_rg(17)                        <= QSFP_C_RESETL_reg;   -- module reset
# data_in_rg(18)                        <= QSFP_C_MODPRSL;      -- module present
# data_in_rg(19)                        <= QSFP_C_INTL;         -- interruption
# data_in_rg(20)                        <= QSFP_C_LPMODE_reg;   -- low power mode   

# data_in_rg(24)                        <= QSFP_D_MODSEL_reg;   -- module select
# data_in_rg(25)                        <= QSFP_D_RESETL_reg;   -- module reset
# data_in_rg(26)                        <= QSFP_D_MODPRSL;      -- module present
# data_in_rg(27)                        <= QSFP_D_INTL;         -- interruption
# data_in_rg(28)                        <= QSFP_D_LPMODE_reg;   -- low power mode   

# data_in_rg(29)                        <= QSFP_E_MODSEL_reg;   -- module select
# data_in_rg(30)                        <= QSFP_E_RESETL_reg;   -- module reset
# data_in_rg(31)                        <= QSFP_E_MODPRSL;      -- module present
# data_in_rg(32)                        <= QSFP_E_INTL;         -- interruption
# data_in_rg(33)                        <= QSFP_E_LPMODE_reg;   -- low power mode   

dth.write64("qsfp_monitoring", 0x0101010101) 
time.sleep(0.1)
# realise RESET on QSFP28
ena_qsfp = 0x0202020202
ena_qsfp_control = 0x0303030303
mask_ena = 0x01
dth.write64("qsfp_monitoring", ena_qsfp) 

# read QSFP status 
QSFP_present = dth.read64("qsfp_monitoring", 0)
print("QSFP status : (b0:CS-b1:Reset-b2:Present-b3:Int-b4:LPower(each 8b))", hex(int(QSFP_present)))

# Check if DTH seen each QSFP
if QSFP_present & 0x0404040404 == 0x0:
    print("All QSFP are seen")
else:
    if QSFP_present & 0x04 != 0x0:
        print("QSFP0 is not seen!")
    if QSFP_present & 0x0400 != 0x0:
        print("QSFP1 is not seen!")
    if QSFP_present & 0x040000 != 0x0:
        print("QSFP2 is not seen!")
    if QSFP_present & 0x04000000 != 0x0:
        print("QSFP3 is not seen!")
    if QSFP_present & 0x0400000000 != 0x0:
        print("QSFP4 is not seen!")

# variable used to stop when a problem has been seen
continu = False
# variable to know is the I2C switch is working
problem_swI2C = False

# variable to control Reset of 1 QSFP at the time
QSFP_reset = 0x0000000002  # used with not  
# variable used to control LPmode the QSFP
QSFP_LPmode = 0x10
# variable used to control INT QSFP
QSFP_INT = 0x08  # used with not

# for each QSFP we check: MODSEL; I2C; Reset;  LPmode; INT
for x in range(5):
    print("==============   QSFP  %x tests  ==================" % x)
    if QSFP_present & 0x04 == 0:
        # MODSEL-- at 0 by default  then check I2C access        
        enable_QSFP = ena_qsfp_control ^ mask_ena
        if (dbg):print("Ena QSPF : ", hex(enable_QSFP))
        dth.write64("qsfp_monitoring", enable_QSFP) 
        
        # First select the QSFP
        if (dbg):print("Select QSFP : ", hex(switch_TCA[x]))
        try:
            i2c_switch.write(0x0, switch_TCA[x])
        except Exception as b:
            problem_swI2C = True
            print("Problem on the I2C switch of QSFP : ", str(b))
            print("All QSFP+ tests will be bypassed")
                
        if not (problem_swI2C):
            
            try:
                QSFP_val = i2c_QSFP.read(0) 
                print("Modsel and I2C work", hex(QSFP_val))
                continu = True  # we continue , we assume that I2C works
            except Exception as a:
                print("The Modsel or the I2C bus doesn't work!!", str(a))
            
            if continu:
                #########################################################
                # check the reset
                i2c_QSFP.write(86, 0x0F)  # disable TX
                QSFP_val = i2c_QSFP.read(86)
                if (dbg):print("TX Disable : ", hex(QSFP_val)) 
                
                cmd_reset = ena_qsfp & (QSFP_reset ^ 0xFFFFFFFFFF)  # Generate RESET on QSFP+
                if (dbg):print("reset : ", hex(cmd_reset))
                
                # reset QSFP
                dth.write64("qsfp_monitoring", cmd_reset)
                time.sleep(0.1)  # need by the QSFP+ module
                dth.write64("qsfp_monitoring", ena_qsfp)
                time.sleep(0.5)  # need by the QSFP+ module
                
                # Check if after ResetL the TX_disable register is reset?
                QSFP_val = i2c_QSFP.read(86)
                if (dbg):print("TX Disable : ", hex(QSFP_val))
                if QSFP_val == 0:
                    print("QSFP reset pin works")
                else:
                    print("There is a problem with the QSFP reset pin!")
                
                # ########################################################
                # check the LPmode pin
                # read the TX power  POwer ON
                TX_power_low = i2c_QSFP.read(51)
                TX_power_high = i2c_QSFP.read(50)
                TX_power_Pon = ((TX_power_high * 256) + TX_power_low) * bit_scale
                if (dbg):print("Power : ", TX_power_Pon)
                
                cmd_LPmode = ena_qsfp | QSFP_LPmode
                dth.write64("qsfp_monitoring", cmd_LPmode)  # LPmode QSFP 
                if (dbg):print("LPmode : ", hex(cmd_LPmode))
                time.sleep(0.3)  # need by the QSFP+ module
                
                # read the TX power  LPmode ON
                TX_power_low = i2c_QSFP.read(51)
                TX_power_high = i2c_QSFP.read(50)
                TX_power_Poff = ((TX_power_high * 256) + TX_power_low) * bit_scale
                if (dbg):print("Power : ", TX_power_Poff)
                
                if TX_power_Pon > 500 and TX_power_Poff < 50:
                    print("LPmode pin works")
                else:
                    print("LPmode pin doens't work!!")
                
                # release the LPmode
                dth.write64("qsfp_monitoring", ena_qsfp)
                time.sleep(0.3)  # need by the QSFP+ module
                
                # ########################################################
                # Check the INT pin of the QSFP
                
                # read Intl before disable CDR
                read_Intl_0 = dth.read64("qsfp_monitoring") & QSFP_INT  # int should be there
                
                # disable CDR which cause the IntL
                i2c_QSFP.write(98, 0x0)  # disable CDR
                time.sleep(1)  # need by the QSFP+ module
                val_QSFP = i2c_QSFP.read(5)
                 
                time.sleep(1)  # need by the QSFP+ module
                # read Intl after disable CDR
                read_Intl_1 = dth.read64("qsfp_monitoring") & QSFP_INT  # int should be there
                
                if read_Intl_0 == 0 and read_Intl_1 != 0:
                    print("IntL pin works")
                else:
                    print("IntL pin doens't work!!")
             
    
    continu = False                     # set for next QSFP
    QSFP_present = QSFP_present >> 8    # go to next QSFP
    QSFP_reset   = QSFP_reset   << 8    # select reset of the next QSFP
    QSFP_LPmode  = QSFP_LPmode  << 8   # select LPmode of the next QSFP
    QSFP_INT     = QSFP_INT     << 8      # select INT of the next QSFP
    mask_ena     = mask_ena     << 8      # select ENA module of the next QSFP
        
        
####################################################################################################
# time.sleep(1)

# for x in range(0,5):
#     print ("------ Access QSFP no : " , str(x) , "------")
#     # access the TCA9548 for clock control
#     i2c_switch.write(0x0,switch_TCA[x]) 
    
#     temp_low    = i2c_QSFP.read(23) 
#     temp_high   = i2c_QSFP.read(22)
#     temp        = ((temp_high  * 256) + temp_low  ) / 256
#     print ("QSFP temperatrure : ", int(temp))
    
#     for y in range(0,4):
#         # RX power
#         # print (hex(y))
#         RX_power_low    = i2c_QSFP.read(35 + (2*y))
#         RX_power_high   = i2c_QSFP.read(34 + (2*y))
#         RX_power        = ((RX_power_high * 256) + RX_power_low ) * bit_scale
#         print ("Lane : ", y , "receive uW : ", int(RX_power))
         

#     for y in range(0,4):
#         # TX power
#         TX_power_low    = i2c_QSFP.read(51 + (2*y)) 
#         TX_power_high   = i2c_QSFP.read(50 + (2*y))
#         TX_power        = ((TX_power_high * 256) + TX_power_low ) * bit_scale
#         print ("Lane : ", y , "transmit uW : ", int(TX_power))
         
 
 
    
