#!/usr/bin/python3

import sys
import os
import select
import time
import itertools
import hal
import json
import numpy as np

from pprint import pprint
from I2CInterface import I2CInterface
from consolemenu import *
from consolemenu.items import *
from mylib.Utility_lib import *
from threading import Thread

VENDID = 0x10dc
DEVID  = 0x01b5
BUSINTERFACE = os.getenv( "BUSINTERFACE" ) # "AXI" or "PCI"
BOARDTYPE = os.getenv( "BOARDTYPE" ) # "DTH-400" or "DAQ-800"
DAQUNIT = os.getenv( "DAQUNIT" ) # 0 or 1
RESOURCEDIR  = os.getenv( "RESOURCEPATH" )
if BUSINTERFACE == "AXI":
    if DAQUNIT == "0":
        AXIADR = "a0000000"
    elif DAQUNIT == "1":
        AXIADR = "a0800000"
    else:
        print("ERROR in env.sh: DAQUNIT must be 0 or 1")
        sys.exit()
    from mylib.QSFP_PRBS_lib import QSFP_PRBS
    from mylib.QSFP_ctrl_lib import QSFP_Ctrl
    ADDRESSTABLE   = os.path.join(RESOURCEDIR, "DTHAddressMap.dat")
    ADDRESSOFFSETS = os.path.join( RESOURCEDIR, 'DTHAddressOffset.json')
    axibase        = "axi_chip2chip@" + AXIADR
    print( "Axi base is %s" % axibase )
    max_qsfps = 5
elif BUSINTERFACE == "PCI":
    from mylib.QSFP_PRBS_pci_lib import QSFP_PRBS
    from mylib.QSFP_ctrl_pci_lib import QSFP_Ctrl
    ADDRESSTABLE   = os.path.join(RESOURCEDIR, "DevBoardAddressMap_pci.dat")
    ADDRESSOFFSETS = os.path.join( RESOURCEDIR, 'DTHAddressOffset_pci.json')
    max_qsfps = 4
else:
    print( "No such businterface: %s" % BUSINTERFACE)
    sys.exit()




root = os.getenv( "ROOTDIR" )
scanresultdir = os.path.join(root, "scan_results" )

if not os.path.isdir( scanresultdir ):
    os.mkdir( scanresultdir )
    print ("Made directory %s" % scanresultdir )

time.sleep(1)


def replacement(arg):
    print("clear")

Screen.clear = replacement
os.system('clear')

def resetBoard():
    dth.write("reset_hw_comp",0xF0000000)
    time.sleep(0.1)
    dth.write("reset_hw_comp",0x00000000)
    time.sleep(0.1)

##################################################################

def ENA_QSFP():

    #   remove local reset
    resetBoard()

    my_QSFP_control.Enable_QSFP()


    done = input("return")
    return

##################################################################

def Setup_PRBS():

    # read if QSFP is present?
    QSFP_present =  dth.read64("qsfp_monitoring",0)
    print("QSFP present ? Each QSFP is a byte (bit 2 = 0?): ",hex(QSFP_present))
    for x in range(0,max_qsfps):
        if (QSFP_present & 0x4 == 0):
            my_QSFP_PRBS_control.QSFP_PRBS_on(QSFP_offset[x])

        QSFP_present = QSFP_present >> 8

    done = input("return")
    return

##################################################################

def Reset_PRBS():

    # read if QSFP is present?
    QSFP_present =  dth.read64("qsfp_monitoring",0)

    for x in range(0,max_qsfps):
        if (QSFP_present & 0x4 == 0):
            my_QSFP_PRBS_control.Reset_PRBS(QSFP_offset[x])

        QSFP_present = QSFP_present >> 8

##################################################################
def PRBS_locked():

    # read if QSFP is present?
    QSFP_present =  dth.read64("qsfp_monitoring",0)

    for x in range(0,max_qsfps):
        if (QSFP_present & 0x4 == 0):
            my_QSFP_PRBS_control.PRBS_locked(x,QSFP_offset[x])

        QSFP_present = QSFP_present >> 8

    done = input("return")
    return

##################################################################
def Read_results():

    Time_base = time.time()

    result = 0

    while 1:
        os.system ('clear')


        # read if QSFP is present?
        QSFP_present =  dth.read64("qsfp_monitoring",0)

        for x in range(0,max_qsfps):
            if (QSFP_present & 0x4 == 0):
                result = my_QSFP_PRBS_control.Read_PRBS_results_on_time(x,True,QSFP_offset[x])

        QSFP_present = QSFP_present >> 8

        seconds2 = time.time()
        diff_sec = seconds2-Time_base
        berr = "{:.2e}".format(1/(diff_sec * 25000000000))

        print ("Diff time : ",berr )
        time.sleep(1)

        if isData():
                break

    done = input("return")
    return

##################################################################
def gen_error():

    # read if QSFP is present?
    QSFP_present =  dth.read64("qsfp_monitoring",0)

    for x in range(0,max_qsfps):
        if (QSFP_present & 0x4 == 0):
            my_QSFP_PRBS_control.PRBS_generate_error(QSFP_offset[x])

        QSFP_present = QSFP_present >> 8

##################################################################
def Remove_PRBS():

    # read if QSFP is present?
    QSFP_present =  dth.read64("qsfp_monitoring",0)

    for x in range(0,max_qsfps):
        if (QSFP_present & 0x4 == 0):
            my_QSFP_PRBS_control.QSFP_PRBS_off(QSFP_offset[x])

        QSFP_present = QSFP_present >> 8

##################################################################
def QSFP_power():

    my_QSFP_control.RxPower_QSFP()


    done = input("return")
    return

##################################################################
def QSFP_temp():

    my_QSFP_control.Temp_QSFP()


    done = input("return")
    return

##################################################################
def Change_analog():

    print("Not yet implemented")

##################################################################
def DumpInfo():

    for i in range(0,max_qsfps):
        print( "QSFP in slot %d" % i)
        print( "===============" )

        my_QSFP_control.dumpInfo(i)
        
    done = input("return")
    return

def scanVDiff():
    i_qsfp = int(input("QSFP number :"))
    if not my_QSFP_control.QSFP_present(i_qsfp):
        print("QSFP %d not present" % i_qsfp )
        input()
        return -1

    pre  = int(input("Enter precursor [0-31] : "))
    post = int(input("Enter postcursor [0-31] : "))
    
    offset = QSFP_offset[i_qsfp]
    # start the PRBS test to get the machinery running
    my_QSFP_PRBS_control.QSFP_PRBS_on(offset)
    time.sleep(0.1)
    # check that the links are locked
    locked = my_QSFP_PRBS_control.PRBS_locked( i_qsfp, offset )
    if locked != 0xF:
        my_QSFP_PRBS_control.QSFP_PRBS_off(offset)
        print( "The links did not lock for the PRBS test: %x" % locked )
        print( "Solve the problem and try again.")
        input()
        return
    
    line_mask = 0xf
    #pre_l = range( 0,32 )
    #post_l = range( 0,32 )
    #pre = 0
    #post = 0
    #list = [pre_l, post_l]
    diff_v = 0x16
    #combinations = [p for p in itertools.product(*list)]
    for diff_v in range(0, 32):
        params = { "pre" : pre, "post" : post, "diff_v" : diff_v }
        my_QSFP_PRBS_control.setLinkParams( offset, line_mask, params )
        time.sleep(0.1)
        # recheck the lock
        locked = my_QSFP_PRBS_control.PRBS_locked( i_qsfp, offset )
        if locked != 0x0F:
            print( "pre %2d post %2d diff_v %2d    UNLOCKED" % (pre,post,diff_v) )
            continue
        # reset the counters of the prbs test
        my_QSFP_PRBS_control.Reset_PRBS( offset )
        # read test results of running test
        time.sleep(0.2)
        print( "pre %2d post %2d diff_v %2d" % (pre,post,diff_v), end="    " )
        res = my_QSFP_PRBS_control.Read_PRBS_results_on_time(i_qsfp,0,offset)
        print( "Errors %10d  %10d  %10d  %10d" % (res[0],res[1],res[2],res[3]) )
        my_QSFP_PRBS_control.QSFP_PRBS_off(offset)
    # stop the PRBS test
    my_QSFP_PRBS_control.QSFP_PRBS_off(offset)
    input()

def fullScanPerLine( i_qsfp, dth_sn = None ):

    offset = QSFP_offset[i_qsfp]
    # check if present
    if not my_QSFP_control.QSFP_present(i_qsfp):
        print("QSFP %d not present" % i_qsfp )
        input()
        return -1

    # build a filename where to save the result
    info = my_QSFP_control.getInfo( i_qsfp )
    #pprint(info)
    # name should be Vendor_PN_SN_Slot.csv
    vendor = info['Vendor Name'].replace(" ", "")
    print ("Vendor is %s" % vendor)
    pn = info['Vendor PN'].replace(" ", "")
    print ("Vendor PN is %s" % str(pn))
    sn = info['Vendor SN'].replace(" ", "")
    print ("Vendor SN is %s" % str(sn))
    slot = i_qsfp
    if dth_sn == None:
        dth_sn =  readSN( dth )
    print ("DTH SN is %s" % dth_sn)
    try:
        dth_sn = "%012x" % dth_sn
    except:
        pass
    csv_filename = "_".join( [vendor,pn,sn,str(slot),dth_sn]) + ".csv"
    info_filename = "_".join( [vendor,pn,sn,str(slot),dth_sn,"info"]) + ".json"

    print( "filenames: %s and %s " % ( csv_filename, info_filename ) )
    
    ifpath = os.path.join( scanresultdir, info_filename )

    fdi = open(ifpath,"w")
    json.dump( info, fdi )
    fdi.close

    cfpath = os.path.join( scanresultdir, csv_filename )
    fdi = open( cfpath, "w" )
    fdi.write( "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % ("delta_t", "diff_v", "pre", "post", "lock", "err_0", "err_1", "err_2", "err_3","berr" ))
    # start the PRBS test to get the machinery running
    my_QSFP_PRBS_control.QSFP_PRBS_on(offset)
    time.sleep(0.1)
    line_mask = 0xf

    ########################### Paramters of the scan ###############################
    t_meas_min = 0.2
    t_meas_max = 2.0    # 4 to reach berr=10e-11
    berr_cut   = 1.0e-2 # if all four lines have at least this berr stop the test.
    #################################################################################
    
    pre_l = range( 0,32 )
    post_l = range( 0,32 )
    diff_v_l = range( 0,32 )
    list = [diff_v_l, pre_l, post_l]
    combinations = [p for p in itertools.product(*list)]
    for comb in combinations:
        params = {  "diff_v" : comb[0], "pre" : comb[1], "post" : comb[2] }
        my_QSFP_PRBS_control.setLinkParams( offset, line_mask, params )
        # reset the counters of the prbs test (This unlocks and we can check if it re-locks)
        time.sleep(0.01) # Necessary to let settle the parameters before the reset.
        my_QSFP_PRBS_control.Reset_PRBS( offset )
        time.sleep(0.01)
        #res = my_QSFP_PRBS_control.Read_PRBS_results_on_time(i_qsfp,0,offset)
        # recheck the lock after a small delay to give time to detect the lock
        locked = my_QSFP_PRBS_control.PRBS_locked( i_qsfp, offset, 0 ) 
        if locked != 0x0F:
            print( "%d : diff_v %2d pre %2d post %2d    UNLOCKED 0x%x" % (i_qsfp,comb[0],comb[1],comb[2], locked) )
            if locked == 0x00:
                fdi.write( "%10.6f,%d,%d,%d,%d,%d,%d,%d,%d,%e\n" % (0, comb[0], comb[1], comb[2], locked, -1,-1,-1,-1,0.0))
                continue

        # read test results of running test
        start = time.time()
        stop = False
        while ( not stop ):
            time.sleep(t_meas_min)
            errs = np.array(my_QSFP_PRBS_control.Read_PRBS_results_on_time(i_qsfp,0,offset))
            lap = time.time()
            dt = lap - start
            errs = np.clip( errs, 1, None ) 
            berrs = errs / (dt * 25.0e9)

            # Criteria to stop the test:
            # All BERRS worse than 1e-3 or time limit
            if dt >= t_meas_max:
                stop = True
            elif np.min(berrs) > berr_cut:
                stop = True

        if stop:
            #print("")
            print( "%d : diff_v %2d pre %2d post %2d" % (i_qsfp,comb[0],comb[1],comb[2]), end="    " )
            print( "Errors %10d  %10d  %10d  %10d    berr %.2e" % (errs[0],errs[1],errs[2],errs[3],np.max(berrs)) )
            fdi.write( "%10.6f,%d,%d,%d,%d,%d,%d,%d,%d,%e\n" % (dt, comb[0], comb[1], comb[2], locked, errs[0],errs[1],errs[2],errs[3],np.max(berrs)))
            fdi.flush()
    # stop the PRBS test
    fdi.close()
    my_QSFP_PRBS_control.QSFP_PRBS_off(offset)
    input()

    
def fullScan( i_qsfp ):

    offset = QSFP_offset[i_qsfp]
    # check if present
    if not my_QSFP_control.QSFP_present(i_qsfp):
        print("QSFP %d not present" % i_qsfp )
        input()
        return -1

    # build a filename where to save the result
    info = my_QSFP_control.getInfo( i_qsfp )
    #pprint(info)
    # name should be Vendor_PN_SN_Slot.csv
    vendor = info['Vendor Name'].replace(" ", "")
    print ("Vendor is %s" % vendor)
    pn = info['Vendor PN'].replace(" ", "")
    print ("Vendor PN is %s" % str(pn))
    sn = info['Vendor SN'].replace(" ", "")
    print ("Vendor SN is %s" % str(sn))
    slot = i_qsfp
    dth_sn =  readSN( dth )
    print ("DTH SN is %s" % dth_sn)
    try:
        dth_sn = "%012x" % dth_sn
    except:
        pass
    csv_filename = "_".join( [vendor,pn,sn,str(slot),dth_sn]) + ".csv"
    info_filename = "_".join( [vendor,pn,sn,str(slot),dth_sn,"info"]) + ".json"

    print( "filenames: %s and %s " % ( csv_filename, info_filename ) )
    
    ifpath = os.path.join( scanresultdir, info_filename )

    fdi = open(ifpath,"w")
    json.dump( info, fdi )
    fdi.close

    cfpath = os.path.join( scanresultdir, csv_filename )
    fdi = open( cfpath, "w" )
    fdi.write( "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % ("delta_t", "diff_v", "pre", "post", "lock", "err_0", "err_1", "err_2", "err_3","berr" ))
    # start the PRBS test to get the machinery running
    my_QSFP_PRBS_control.QSFP_PRBS_on(offset)
    time.sleep(0.1)
    line_mask = 0xf

    ########################### Paramters of the scan ###############################
    t_meas_min = 0.2
    t_meas_max = 0.5
    max_err = 20 # interrupt measurement if so many errors are found
    berr_lim = 1.0e-11
    #################################################################################
    
    pre_l = range( 0,32 )
    post_l = range( 0,32 )
    diff_v_l = range( 0,32 )
    list = [diff_v_l, pre_l, post_l]
    combinations = [p for p in itertools.product(*list)]
    for comb in combinations:
        params = {  "diff_v" : comb[0], "pre" : comb[1], "post" : comb[2] }
        my_QSFP_PRBS_control.setLinkParams( offset, line_mask, params )
        # reset the counters of the prbs test (This unlocks and we can check if it re-locks)
        time.sleep(0.01) # Necessary to let settle the parameters before the reset.
        my_QSFP_PRBS_control.Reset_PRBS( offset )
        time.sleep(0.01)
        #res = my_QSFP_PRBS_control.Read_PRBS_results_on_time(i_qsfp,0,offset)
        # recheck the lock after a small delay to give time to detect the lock
        locked = my_QSFP_PRBS_control.PRBS_locked( i_qsfp, offset, 0 ) 
        if locked != 0x0F:
            print( "%d : diff_v %2d pre %2d post %2d    UNLOCKED 0x%x" % (i_qsfp,comb[0],comb[1],comb[2], locked) )
            fdi.write( "%10.6f,%d,%d,%d,%d,%d,%d,%d,%d,%e\n" % (0, comb[0], comb[1], comb[2], locked, -1,-1,-1,-1,0.0))
            continue

        # read test results of running test
        start = time.time()
        stop = False
        berr = 0.0
        while ( not stop ):
            time.sleep(t_meas_min)
            res = my_QSFP_PRBS_control.Read_PRBS_results_on_time(i_qsfp,0,offset)
            errsum = (res[0]+res[1]+res[2]+res[3])
            lap = time.time()            
            dt = lap - start
            if errsum == 0:
                errsum += 1
            berr = errsum / (dt * 25.0e9)
            if errsum > max_err:
                # stop the test here
                fdi.write( "%10.6f,%d,%d,%d,%d,%d,%d,%d,%d,%e\n" % (dt, comb[0], comb[1], comb[2], locked, res[0],res[1],res[2],res[3],berr))
                stop = True
                continue

            if berr <= berr_lim:
                # stop the test here
                fdi.write( "%10.6f,%d,%d,%d,%d,%d,%d,%d,%d,%e\n" % (dt, comb[0], comb[1], comb[2], locked, res[0],res[1],res[2],res[3],berr))
                stop = True
                continue

            if dt >= t_meas_max:
                # stop the test here
                fdi.write( "%10.6f,%d,%d,%d,%d,%d,%d,%d,%d,%e\n" % (dt, comb[0], comb[1], comb[2], locked, res[0],res[1],res[2],res[3],berr))
                stop = True
                continue
                
        if stop:
            #print("")
            print( "%d : diff_v %2d pre %2d post %2d" % (i_qsfp,comb[0],comb[1],comb[2]), end="    " )
            print( "Errors %10d  %10d  %10d  %10d    berr %.2e" % (res[0],res[1],res[2],res[3],berr) )
            fdi.flush()
    # stop the PRBS test
    fdi.close()
    my_QSFP_PRBS_control.QSFP_PRBS_off(offset)
    input()

    
def scanParameter( ):

    i_qsfp = int(input("QSFP number :"))
    
    offset = QSFP_offset[i_qsfp]
    # check if present
    if not my_QSFP_control.QSFP_present(i_qsfp):
        print("QSFP %d not present" % i_qsfp )
        input()
        return -1

    # start the PRBS test to get the machinery running
    my_QSFP_PRBS_control.QSFP_PRBS_on(offset)
    time.sleep(0.1)
    ## check that the links are locked
    #locked = my_QSFP_PRBS_control.PRBS_locked( i_qsfp, offset )
    #if locked != 0xF:
    #    my_QSFP_PRBS_control.QSFP_PRBS_off(offset)
    #    print( "The links did not lock for the PRBS test: %x" % locked )
    #    print( "Solve the problem and try again.")
    #    input()
    #    return
    
    line_mask = 0xf
    pre_l = range( 0,32 )
    post_l = range( 0,32 )
    list = [pre_l, post_l]
    diff_v = 0x08
    combinations = [p for p in itertools.product(*list)]
    for comb in combinations:
        params = { "pre" : comb[1], "post" : comb[0], "diff_v" : diff_v }
        my_QSFP_PRBS_control.setLinkParams( offset, line_mask, params )
        # reset the counters of the prbs test (This unlocks and we can check if it re-locks)
        time.sleep(0.1) # Necessary to let settle the parameters before the reset.
        my_QSFP_PRBS_control.Reset_PRBS( offset )
        time.sleep(0.1)
        #res = my_QSFP_PRBS_control.Read_PRBS_results_on_time(i_qsfp,0,offset)
        # recheck the lock after a small delay to give time to detect the lock
        locked = my_QSFP_PRBS_control.PRBS_locked( i_qsfp, offset, 0 ) 
        if locked != 0x0F:
            print( "pre %2d post %2d diff_v %2d    UNLOCKED 0x%x" % (comb[0],comb[1],diff_v, locked) )
            continue
        # read test results of running test
        time.sleep(0.2)
        if comb[1] == 0:
            print("")
        print( "pre %2d post %2d" % (comb[0],comb[1]), end="    " )
        res = my_QSFP_PRBS_control.Read_PRBS_results_on_time(i_qsfp,0,offset)
        print( "Errors %10d  %10d  %10d  %10d" % (res[0],res[1],res[2],res[3]) )
    # stop the PRBS test
    my_QSFP_PRBS_control.QSFP_PRBS_off(offset)
    input()

##################################################################
def DoPRBSTest( ):
    resetBoard()
    iqsfp = int(input( "Enter qsfp number : " ))
    
    '''iqsfp goes from 0 to max_qsfps'''
    # check if present
    if not my_QSFP_control.QSFP_present(iqsfp):
        return -1

    ########### Enable the qsfp to be tested
    ena_qsfp = 0x02 << (iqsfp*8)
    enstat = dth.read64("qsfp_monitoring")
    print( "enstat %0x" % enstat )

    ena_qsfp |= enstat
    print("writing %0x" % ena_qsfp )
    dth.write64( "qsfp_monitoring", ena_qsfp )

    enstat = dth.read64("qsfp_monitoring")
    print( "enstat %0x" % enstat )

    # reset the counters of the prbs test
    my_QSFP_PRBS_control.Reset_PRBS( QSFP_offset[iqsfp] )
    # start the PRBS test
    my_QSFP_PRBS_control.QSFP_PRBS_on(QSFP_offset[iqsfp])
    # read test results of running test
    time.sleep(1.0)
    my_QSFP_PRBS_control.Read_PRBS_results_on_time(iqsfp,1,QSFP_offset[iqsfp])
    
    
    input()
    return 0
    
##################################################################
def ScanPrePost():

    resetBoard()
    scanParameter()
    input()

    return

##################################################################
def ScanVDiff():

    resetBoard()
    scanVDiff()
    input()

    return

##################################################################
def PRBS_on():

    i_qsfp = int(input("Enter QSFP [0-4] : "))
    # check if present
    if not my_QSFP_control.QSFP_present(i_qsfp):
        print("QSFP %d not present" % i_qsfp )
        input()
        return -1

    
    offset = QSFP_offset[i_qsfp]
    my_QSFP_PRBS_control.QSFP_PRBS_on(offset)

    
    input()

    return


##################################################################
def PRBS_off():

    for i in range(0,max_qsfps):
        offset = QSFP_offset[i]
        my_QSFP_PRBS_control.QSFP_PRBS_off(offset)
    resetBoard()
    input()

    return


###################################################################
def LoopbackScan():

    qsfp_mask = int(input("Enter QSFP mask: "),0)
    dth_SN = readSN(dth)
    
    activeThreads = []    
    for i in range(0,max_qsfps):
        print( 1<<i, qsfp_mask )
        if ( (1<<i) & qsfp_mask ):
            if ( my_QSFP_control.QSFP_present( i ) ):
                t = Thread( target = fullScanPerLine, args = [i, dth_SN] )
                print("Launching scan for QSFP %d" % i )
                t.start()
                activeThreads.append( t )

    for t in activeThreads:
        t.join()
        print("Thread %s terminated" % str(t) )

    print("All done")

    input()

    return

###################################################################
def I2CTestLoop() :
    print( "i2c endless loop reading fixed no of bytes")
    slot = int(input("Enter slot number : " ))
    if not my_QSFP_control.QSFP_present(slot):
        print("No QSFP plugged in slot %d" %slot)
        input()
        return
    
    my_QSFP_control.i2c_switch.write(0x0,my_QSFP_control.switch_TCA[slot])

    adr = int(input("Enter the start address (register number) : " ),0 )
    length = int(input("Enter the number of registers to read : "), 0 )

    while ( True ):
        for i in range(adr, adr+length):
            c = my_QSFP_control.i2c_QSFP.read(adr)        
    
    input()
    
def VendorLoop() :
    print( "i2c loop read test")
    slot = int(input("Enter slot number : " ))
    if not my_QSFP_control.QSFP_present(slot):
        print("No QSFP plugged in slot %d" %slot)
        input()
        return

    my_QSFP_control.i2c_switch.write(0x0,my_QSFP_control.switch_TCA[slot])

    # time.sleep(0.1)
    # select page 0
    my_QSFP_control.i2c_QSFP.write( 0x7F, 0 )
    #time.sleep(0.1)

    ov = my_QSFP_control.readVendor(slot)

    print("reference : %s" % ov)
    
    while ( True ):
        vname = my_QSFP_control.readVendor(slot)
        if vname != ov:
            print("found %s   length %d" % (vname, len(vname)))
    
    input()
    
def SerialNumber() :
        
    dth_sn =  readSN( dth )
    try:
        dth_sn = "%012x" % dth_sn
    except:
        pass
    print( "serial number of dth or daq800: %s " % dth_sn )
    input()

def Test() :

    for i in range(0,max_qsfps):
        serdes_status = my_QSFP_control.read64( "mac100g_serdes_status", QSFP_offset[i])
        print("qsfp %d  serdes_status : 0x%x" % (i,serdes_status))
        
    dth_sn =  readSN( dth )
    try:
        dth_sn = "%012x" % dth_sn
    except:
        pass
    print( "serial number of dth: %s " % dth_sn )
    input()
###################################################################
# This code is searching for a DTH
#

print("open dth")
if BUSINTERFACE == "AXI":
    dth = hal.AXIDevice(axibase, ADDRESSTABLE)
    my_QSFP_control         = QSFP_Ctrl(axibase, ADDRESSTABLE)
    my_QSFP_PRBS_control    = QSFP_PRBS(axibase, ADDRESSTABLE)
elif BUSINTERFACE == "PCI":
    dth = hal.PCIDevice(ADDRESSTABLE,VENDID,DEVID,0)
    my_QSFP_control         = QSFP_Ctrl(ADDRESSTABLE,VENDID,DEVID,0)
    my_QSFP_PRBS_control    = QSFP_PRBS(ADDRESSTABLE,VENDID,DEVID,0)
else:
    print("Unknown bus-interface: %s" % BUSINTERFACE )
    sys.exit()
   
with open(ADDRESSOFFSETS) as json_file:
    DTH_Offset = json.load(json_file)

QSFP_offset = DTH_Offset['daq_eth_100gb']


# Create the root menu
menu = ConsoleMenu("DAQ FireFly control ")

#Setup serdes
function_item1 = FunctionItem("Enable QSFP  ", ENA_QSFP  )
function_item2 = FunctionItem("Setup PRBS  ", Setup_PRBS  )
function_item3 = FunctionItem("Reset PRBS", Reset_PRBS )
function_item4 = FunctionItem("PRBS Locked?", PRBS_locked )
function_item5 = FunctionItem("Read Results", Read_results )
function_item6 = FunctionItem("Generate PRBS error", gen_error )
function_item7 = FunctionItem("Conf normal work (no PRBS)", Remove_PRBS )
function_item8 = FunctionItem("QSFP read power", QSFP_power )
function_item9 = FunctionItem("QSFP read temperature", QSFP_temp )
function_item10 = FunctionItem("Change Analogue values (PreCursors,..", Change_analog )
function_item11 = FunctionItem("Dump QSFP Information", DumpInfo )
function_item12 = FunctionItem("Do a PRBS test", DoPRBSTest )
function_item13 = FunctionItem("Scan pre and post", ScanPrePost )
function_item14 = FunctionItem("Scan V_diff", ScanVDiff )
function_item15 = FunctionItem("PRBS off", PRBS_off )
function_item16 = FunctionItem("PRBS on", PRBS_on )
function_item17 = FunctionItem("Multithreaded Loopback scan for all slots", LoopbackScan )
function_item18 = FunctionItem("Endless loop for i2c reading", I2CTestLoop )
function_item19 = FunctionItem("Endless loop to read Vendor", VendorLoop )
function_item20 = FunctionItem("Read serial number", SerialNumber)
function_item21 = FunctionItem("Test item", Test )

# Add all the items to the root menu

menu.append_item(function_item1)
menu.append_item(function_item2)
menu.append_item(function_item3)
menu.append_item(function_item4)
menu.append_item(function_item5)
menu.append_item(function_item6)
menu.append_item(function_item7)
menu.append_item(function_item8)
menu.append_item(function_item9)
menu.append_item(function_item10)
menu.append_item(function_item11)
menu.append_item(function_item12)
menu.append_item(function_item13)
menu.append_item(function_item14)
menu.append_item(function_item15)
menu.append_item(function_item16)
menu.append_item(function_item17)
menu.append_item(function_item18)
menu.append_item(function_item19)
menu.append_item(function_item20)
menu.append_item(function_item21)

# Show the menu
menu.start()
menu.join()
