#!/usr/bin/python3
import pandas as pd
import numpy as np
import sys
import shutil
import os
import plotly as plt
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from optparse import OptionParser
from glob import glob
from threading import Thread
import math
import re
from pprint import pprint

# Arrange with slots in rows and 4 lines in 4 columns
# One page per measurement

MAXPLOTS=32
# for testing:
# MAXPLOTS=4

imgtype = "png"

respath   = os.getenv('RESOURCEPATH') 
boardtype = os.getenv('BOARDTYPE')
daqunitno = str(os.getenv('DAQUNIT'))
imgpath = os.path.join( os.getenv('ROOTDIR'), "scan_results")
#imgpath = os.path.join( "../")
outpath = os.path.join( imgpath, 'plots_by_line' )

if not os.path.isdir(outpath):
    os.mkdir(outpath)
    shutil.copy( os.path.join(respath, "Remarks.html_inc" ), outpath)
shutil.copy(os.path.join( respath, "prbs.js" ), outpath )
shutil.copy(os.path.join( respath, "prbs.css" ), outpath )
shutil.copy(os.path.join( respath, "jquery.min.js" ), outpath )

    
parser = OptionParser()
parser.add_option("-s", "--skip",
                  action="store_false", dest="do_process", default=False,
                  help="don't process the data but just do the plots")
parser.add_option("-b", "--berr",
                  action="store_false", dest="berr", default=True,
                  help="make plots with log berr as z-axis")
parser.add_option( "-f", "--file", 
                  dest="infile", default="_all_",
                  help="Name of the input file. If set to _all_ process all csv files in the current directory.")

(options, args) = parser.parse_args()



################################################################################################

# re-order the plots to create pages for each slot/line with columns for the different daqunits
# and rows for the different QSFPs

dirs = glob( "scan_*/plots_berr*" )
slots = []
lines = []
daqunits = []
vendors = []
qsfps = []
resh = {}


for d in dirs:
    #print( ">>>>>>>>>>doing " + d)
    mo = re.match( r'scan_\d+/plots_berr_([^_]+)_(.+)_(\d)_([0-9a-zA-Z\-]+)$', d )
    if not mo:
        print( "no match: ", d)
        sys.exit()
    vendor = mo.group(1)
    qsfp = vendor + "_" + mo.group(2)
    slot = int(mo.group(3))
    daqunit = mo.group(4)

    print( "qsfp %s slot %s daqunit %s" % (qsfp,slot,daqunit))
    
    if slot not in resh:
        resh[slot] = { 'qsfp' : qsfp,
                       0 : {},
                       1 : {},
                       2 : {},
                       3 : {} }    

    if qsfp != resh[slot]['qsfp']:
        print( "Error: expected one qsfp for each slot. This script is for configurations where qsfps are not swapped between slots")
        sys.exit()
    if qsfp not in resh[slot][0]:
        resh[slot][0][qsfp] = {}
    if qsfp not in resh[slot][1]:
        resh[slot][1][qsfp] = {}
    if qsfp not in resh[slot][2]:
        resh[slot][2][qsfp] = {}
    if qsfp not in resh[slot][3]:
        resh[slot][3][qsfp] = {}

    resh[slot][0][qsfp][daqunit] = d
    resh[slot][1][qsfp][daqunit] = d
    resh[slot][2][qsfp][daqunit] = d
    resh[slot][3][qsfp][daqunit] = d

    if slot==1 and qsfp == 'JUNIPER-1F2_1F2Q5A_1F2CQ5A6131U8':
        print( slot, qsfp, daqunit, d)
    
    #print( mo.group(0))
    #print (slot, vendor, qsfp, daqunit)
    
    if vendor not in vendors:
        vendors.append(vendor)
    if qsfp not in qsfps:
        qsfps.append(qsfp)
    if slot not in slots:
        slots.append( slot )
    if daqunit not in daqunits:
        daqunits.append( daqunit )

# Not all qsfps have been tested in all slots of all daqunit. 
        
slots.sort()
vendors.sort()
qsfps.sort()
daqunits.sort()
print( "slots  : ", slots)
print( "vendors : ", vendors)
print( "qsfps   : ", qsfps)
print( "daqunits   : ", daqunits)

pprint(resh)

#sys.exit()

def plot_div( fd, config, slot,line,qsfp,daqunit,imgtype="png" ):
    div_0 = "slot_"+str(slot)+"_line_"+str(line)+"_"+qsfp+"_"+daqunit
    fd.write( '<div id="'+ div_0 +'" class="plotdiv">' )
    fd.write( '<div>')    
    if (qsfp in config[slot][line]) and (daqunit in config[slot][line][qsfp]) :
        fd.write( '<input name="' +div_0 + '" class="fullwidth" type="range" min="0" max="31" oninput="changeLinePlot()"/>\n')
        fd.write( '</div>')
        #fd.write( '<h1>' + div_0 + '</h1>')
        #plotdirname = "plots_berr_" + qsfp + "_" + str(slot) + "_" + daqunit
    
        for i in range(0,MAXPLOTS):
            fd.write('<div class="aframe" id="f_' + div_0 + "_" + str(i) + '" >\n')
            fd.write('<h2> V_diff index: '+str(i)+'</h2>')
            plotname = os.path.join( "..",config[slot][line][qsfp][daqunit], "line_"+str(line)+"_out_"+str(i)+".png")
            fd.write( '<image class="prbsplot" src="' + plotname + '">\n')
            fd.write('</div>\n')
    else:
        print( "slot %d line %d qsfp %s, daqunit %s" % (slot, line, qsfp, daqunit))
        fd.write( "n.a." )
    fd.write('</div>\n')

def plotMeasurement( config, daqunit ):

    outname = "daqunit_" + daqunit + ".html"
    #pprint(config)
    #if slot not in config or line not in config[slot]:
    #    print( "Slot %d Line %d does not exist" % (slot, line))
    #    return
    
    fn = os.path.join( outpath, outname )
    fd = open( fn, "w" )
    fd.write( "<!DOCTYPE html>\n" )
    fd.write( '<script charset="utf-8" src="jquery.min.js"></script>\n')
    fd.write( '<script charset="utf-8" src="prbs.js"></script>\n')
    fd.write( '<link rel="stylesheet" href="prbs.css">\n')
    fd.write( '</head>\n' )
    fd.write( '<body onload="initPlot()">' )

    # A header with links to the next slot/line
    hdiv = '<div class="header" >'
    hdiv += '<div id="doc"><span id="docspan" \class="headerMiddle" onclick="$(\'div#documentation\').show()">Documentation<span></div>'
    hdiv += '<input id="globalRuler" type="range" min="0" max="31" oninput="globalChange()" width="200px" />\n';
    hdiv += '</div>\n'    
    fd.write(hdiv)

    fd.write( '<div style="min-height:80px;"> d&nbsp;</div>' )
    fd.write( "<h1> Board : " + boardtype + "</h1>")
    fd.write( "<h2> Daqunit tested : no %s  (serno: %s)</h2>" %(daqunitno, daqunit))
    fd.write( '<table class="plottab">\n' )
    fd.write( '<thead><tr><th class="qsfpcolum">Slot</th>' )
    for line in range (0,4):
        fd.write( '<th class="plotcolumn"> Line ' + str(line) + "</th>") 
    fd.write( '</tr></thead>\n<tbody>')
        
    for slot in range(0,5):
        qsfp = config[slot]['qsfp']
        fd.write( "<tr><td>" + str(slot) + '<br><span style="font-size: 9px;">' + qsfp + "</span></td>" )
        for line in range( 0,4):
            fd.write('<td class="plotcell">')
            plot_div( fd, config, slot, line, qsfp, daqunit )
            fd.write("</td>")
        fd.write( "</tr>\n" )
        
    fd.write("</tbody>\n")
    fd.write( '</table>\n')

    fd.write( '<div id="documentation" onclick="$(\'div#documentation\').hide()">')
    doctxtf = os.path.join( outpath, "Remarks.html_inc" );
    fr = open(doctxtf, 'r')
    doc = fr.read()
    fr.close()

    for repl, val in zip(["boardtype", "daqunit", "daqunitno"],[boardtype, daqunit, daqunitno]):
        doc = doc.replace("%" + repl + "%", val )
    
    fd.write( doc )
    fd.write( '</div>')
    fd.write( '</body>' )
    
    fd.write( '</html>' )
    fd.close()

        
def plotLine( config, slot, line ):
    global daqunits, qsfps
    outname = "slot_" + str(slot) + "_line_" + str(line) + ".html"
    #pprint(config)
    if slot not in config or line not in config[slot]:
        print( "Slot %d Line %d does not exist" % (slot, line))
        return
    
    fn = os.path.join( outpath, outname )
    fd = open( fn, "w" )
    fd.write( "<!DOCTYPE html>\n" )
    fd.write( '<script charset="utf-8" src="jquery.min.js"></script>\n')
    fd.write( '<script charset="utf-8" src="prbs.js"></script>\n')
    fd.write( '<link rel="stylesheet" href="prbs.css">\n')
    fd.write( '</head>\n' )
    fd.write( '<body onload="initPlot()">' )

    # A header with links to the next slot/line
    hdiv = '<div class="header" >'
    hdiv += '<span id=buttons" onchange="selectPlot()"> Slots: '
    for i in range(0,5):
        hdiv += str(i)+':<input type="radio" name="slotchooser" value="'+str(i)+'"/> &nbsp;&nbsp;'
    hdiv += '&nbsp;&nbsp;&nbsp;&nbsp;Lines: '
    for i in range(0,4):
        hdiv += str(i)+':<input type="radio" name="linechooser" value="'+str(i)+'"/> &nbsp;&nbsp;&nbsp;&nbsp;'
    hdiv += "</span>";
    hdiv += '<div id="doc"><span id="docspan" \class="headerMiddle" onclick="$(\'div#documentation\').show()">Documentation<span></div>'
    hdiv += '<input id="globalRuler" type="range" min="0" max="31" oninput="globalChange()" width="200px" />\n';
    hdiv += '</div>\n'    
    fd.write(hdiv)

    fd.write( '<div style="min-height:80px;"> d&nbsp;</div>' )
    fd.write( '<table class="plottab">\n' )
    fd.write( '<thead><tr><th class="qsfpcolum"></th>' )
    
    for daqunit in daqunits:
        fd.write( '<th class="plotcolum">'+daqunit+"</th>" )
    fd.write('</tr></thead>\n' )
    fd.write("<tbody>")
    # important here to loop over all daqunits and qsfps which exist
    # the check if a plot exists for a given line/slot comes later
    qsfps = sorted(qsfps)
    for qsfp in qsfps:
        daqunits = sorted(daqunits)
        fd.write('<tr><td>' + qsfp + '</td>')
        for daqunit in daqunits:
            fd.write('<td class="plotcell">')
            plot_div( fd, config, slot, line, qsfp, daqunit )
            fd.write( '</td>' )
        fd.write( '</tr>\n' )
                
    fd.write("</tbody>\n")
    
    fd.write( '</table>\n')

    fd.write( '<div id="documentation" onclick="$(\'div#documentation\').hide()">')
    doctxtf = os.path.join( outpath, "Remarks.html_inc" );
    fr = open(doctxtf, 'r')
    doc = fr.read()
    fr.close()
    fd.write( doc )
    fd.write( '</div>')
    fd.write( '</body>' )
    
    fd.write( '</html>' )
    fd.close()


# Create the pages
for daqunit in daqunits:
    plotMeasurement( resh, daqunit )    

