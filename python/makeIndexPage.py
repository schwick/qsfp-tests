#!/usr/bin/python3
import sys
import re
import os
import json
import shutil

from glob import glob

respath = os.getenv('RESOURCEPATH')

scandir = os.path.join(os.getenv('ROOTDIR'),'scan_results')

tmpfile = os.path.join( respath, "indexPage.htmltmp")

shutil.copy(os.path.join( respath, "index.css" ), "./" )

indexpage = "index.html"

with open( tmpfile ) as fd:
    html = fd.read()

scandirs = sorted(glob( os.path.join( scandir, "scan_*" )))

listhtml = "<table class='list_tab'>\n"
listhtml += "<thead><tr><th>plots</th><th>scan type</th></tr></thead>\n"
listhtml += "<tbody>\n"
i = 0
for sdir in scandirs:
    infofile = os.path.join(sdir,'info.json')
    if not os.path.isfile( infofile ):
        continue
    fd = open(infofile,"r")
    info = json.load(fd)
    fd.close()
    #print( info['info'])
    print("")
    i += 1

    plotdirs = sorted(glob( os.path.join( sdir, "plots_berr_*" )))
    plot_tab =  '<table class="plottab"><thead><tr>'
    plot_tab += "<th>Slot</th><th>Vendor</th><th>Device</th><th>serialno.</th><th>board id</th></tr>\n"
    plot_tab += "</tr></thead><tbody>\n"
    for plot in plotdirs:
        mo = re.match(r'.*plots_berr_([^_]*)_([^_]*)_([^_]*)_([0-9]*)_(.*)', plot)
        if not mo:
            print( "regex for plot dir name did not match: ", plot )
            sys.exit()

        link = os.path.join( sdir, plot, "out.html" )
        print(link)
        vendor = mo.group(1)
        device = mo.group(2)
        serial_no = mo.group(3)
        slot = mo.group(4)
        board_id = mo.group(5)

        plot_tab += '<tr onclick="window.open(\''+link+'\', \'_blank\');"><td>' + str(slot) + "</td>" + \
            "<td>" + vendor + "</td>" + \
            "<td>" + device + "</td>" + \
            "<td>" + serial_no + "</td>" + \
            "<td>" + board_id + "</td>" + \
            "</tr>\n"

    plot_tab += "</tbody></table>\n"
        
    listhtml += " <tr><td>"+ plot_tab +"</td><td>" + info['info'] + "</td></tr>\n"
    #listhtml += "<tr><td>" + info['info'] + "</td></tr>\n"
    #listhtml += " <tr><td>" + plot_tab +  "</td></tr>\n"

listhtml += "</tbody></table>\n"

html = re.sub(r'__body__',listhtml,html,0,re.MULTILINE)


fd = open(indexpage, "w")
fd.write(html)
fd.close()

