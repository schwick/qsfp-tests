#!/usr/bin/python
import sys
import os
import select
import time
import hal
from I2CInterface import I2CInterface

__version__ = "01.00.00"

ADDRESSTABLE_NAME = 'DTHAddressMap.dat'

class FF_Ctrl( hal.AXIDevice ):
    '''Comment
    '''

    def __init__( self, deviceName, adressTablePath):
        '''The constructor of the DTH class

        '''

        # Initialise the i2c interfaces of the dth
        self.i2c_switch  = I2CInterface(self,0xE0, offsetWidth = 0x0 , dataWidth = 0x1,
                                        items={'i2c_a': 'firefly_i2c_access_a', 'i2c_b': 'firefly_i2c_access_b','i2c_poll': 'firefly_i2c_access_a_access_done'})
        self.i2c_FireFly = I2CInterface(self,0xA0, offsetWidth = 0x1 , dataWidth = 0x1,
                                        items={'i2c_a': 'firefly_i2c_access_a', 'i2c_b': 'firefly_i2c_access_b','i2c_poll': 'firefly_i2c_access_a_access_done'})

        #  This parameter specifies the switch Output order for FireFly 0,1,2,3,4,5
        self.switch_TCA = [0x1,0x2,0x4,0x8,0x10,0x20]
        
        self.text_time =["0 °C ≤ %d min at Temperature < 5 °C",
                         "5 °C ≤ %d min at Temperature < 10 °C",
                         "10 °C ≤ %d min at Temperature < 15 °C",
                         "15 °C ≤ %d min at Temperature < 20 °C",
                         "20 °C ≤ %d min at Temperature < 25 °C",
                         "25 °C ≤ %d min at Temperature < 30 °C",
                         "30 °C ≤ %d min at Temperature < 35 °C",
                         "35 °C ≤ %d min at Temperature < 40 °C",
                         "40 °C ≤ %d min at Temperature < 45 °C",
                         "45 °C ≤ %d min at Temperature < 50 °C",
                         "50 °C ≤ %d min at Temperature < 55 °C",
                         "55 °C ≤ %d min at Temperature < 60 °C",
                         "60 °C ≤ %d min at Temperature < 65 °C",
                         "65 °C ≤ %d min at Temperature < 70 °C",
                         "70 °C ≤ %d min at Temperature < 75 °C",
                         "75 °C ≤ %d min at Temperature < 80 °C",
                         "80 °C ≤ %d min at Temperature < 85 °C",
                         "85 °C ≤ %d min at Temperature < 90 °C",
                         "90 °C ≤ %d min at Temperature < 95 °C",
                         "95 °C ≤ %d min at Temperature < 100 °C",
                         " %d min at Temperature > 100 °C"]



    ##################################################################
    #enable FireFly
    def Enable_FIREFLY(self):

        FF_ena  = self.read("firefly_monitoring",0)
        print ("Status FireFlys",hex(FF_ena))
        self.write("firefly_monitoring",0x222222,False,0)
        FF_ena  = self.read("firefly_monitoring",0)
        print ("Status FireFlys after realized reset",hex(FF_ena))



    ##################################################################

    def Set_rate_FIREFLY(self,rate):
        rate_val = 0xFF * int(rate)
        self.i2c_FireFly.write(99,rate_val)

        if rate == '0':
            print("Rate  25 Gbs set")

        if rate == '1':
            print("Rate  28 Gbs set")

    def Set_rate_FIREFLYs(self):
        FF_ena  = self.read("firefly_monitoring",0)
        FF_present  = FF_ena & 0x444444

        rate = input("Which rate to be set ? 25Gb (0) or 28GB(1) ?")

        for x in range(6):
            if (FF_present & 0x4) == 0x0 :
                self.i2c_switch.write(0x0,self.switch_TCA[x])
                #change the Page
                self.i2c_FireFly.write(127,0x0)
                
                self.Set_rate_FIREFLY(rate)
            else:
                print ("FF : ",x, "not present" )
            FF_present = FF_present >> 4

    ##################################################################
    def Set_CDR_FIREFLY(self,enable):
        CDR_val = 0xFF * int(enable)
        self.i2c_FireFly.write(98,CDR_val)

        if enable == 0:
            print("CDR disable")

        if enable == 1:
            print("CDR_enable")


    #CDR settings FireFly
    def Set_CDR_FIREFLYs(self):

        FF_ena  = self.read("firefly_monitoring",0)
        FF_present  = FF_ena & 0x444444
        print ("Would you like to enable or disable ")
        choice = input("E or D ?")

        if (choice == "E" or choice == "e"):
            CDR_ena = 1
        else :
            CDR_ena = 0

        for x in range(6):
            if (FF_present & 0x4) == 0x0 :
                print ("FF is there : ",x )
                self.i2c_switch.write(0x0,self.switch_TCA[x])
                #change the Page
                self.i2c_FireFly.write(127,0x0)
                
                self.Set_CDR_FIREFLY(CDR_ena)
            else:
                print ("FF : ",x, "not present" )

            FF_present = FF_present >> 4

    ##################################################################
    #Temperature FireFly
    def Temp_FIREFLY(self):

        FF_ena  = self.read("firefly_monitoring",0)
        FF_present  = FF_ena & 0x444444
        print ("present : ",hex(FF_present))

        for x in range(6):
            if (FF_present & 0x4) == 0x0 :
                print ("FF number : ",x)
                self.i2c_switch.write(0x0,self.switch_TCA[x])
                #change the Page
                self.i2c_FireFly.write(127,0x0)

                temp = self.i2c_FireFly.read(22)
                print ("temp -----:", temp)
                temp = 0
            else:
                print ("FF : ",x, "not present" )

            FF_present = FF_present >> 4


    ##################################################################
    #Read firmware version FireFly
    def Version_FIREFLY(self):
        #
        FF_ena  = self.read("firefly_monitoring",0)
        FF_present  = FF_ena & 0x444444

        for x in range(6):
            if (FF_present & 0x4) == 0x0 :
                print ("FireFly number : ",x)
                self.i2c_switch.write(0x0,self.switch_TCA[x])
                #change the Page
                self.i2c_FireFly.write(127,0x0)
                print ("Read values...")
                temp = self.i2c_FireFly.read(69)
                print ("Add 69: ", hex(temp))
                temp = self.i2c_FireFly.read(70)
                print ("Add 70: ", hex(temp))
                temp = self.i2c_FireFly.read(71)
                print ("Add 71: ", hex(temp))
            else:
                print ("FF : ",x, "not present" )

            FF_present = FF_present >> 4

    ##################################################################
    #Read Rx poewr FireFly
    def RxPower_FIREFLY(self):
        #
        FF_ena  = self.read("firefly_monitoring",0)
        FF_present  = FF_ena & 0x444444

        for x in range(6):
            if (FF_present & 0x4) == 0x0 :

                self.i2c_switch.write(0x0,self.switch_TCA[x])
                
                #change the Page
                self.i2c_FireFly.write(127,0x0)
                print ("****************************************")
                print ("FireFly number : ",x)
                print ("----------------------" )
                for rx in range(0,4) :
                    rx_power_high   = self.i2c_FireFly.read(34 + (rx * 2))
                    #print ("Rx power MSB offset : " , 34 + (rx * 2), " -- val : ", hex(rx_power_high))
                    rx_power_low    = self.i2c_FireFly.read(35 + (rx * 2))
                    #print ("Rx power LSB offset : " , 35 + (rx * 2), " -- val : ",hex(rx_power_low))
                    rx_power = (rx_power_high * 0x100 + rx_power_low ) * 0.100
                    print ("RX power on link %d  is"% rx, " %d uW: " % rx_power)

            else:
                print ("FF : ",x, "not present" )

            FF_present = FF_present >> 4

    ##################################################################
    #Read time on a specific temperature
    def Temp_time_FIREFLY(self):
        #
        FF_ena  = self.read("firefly_monitoring",0)
        FF_present  = FF_ena & 0x444444

        for x in range(6):
            if (FF_present & 0x4) == 0x0 :
                
                # Select the FIREFLY in TCA chip
                self.i2c_switch.write(0x0,self.switch_TCA[x])
                
                #change the Page
                self.i2c_FireFly.write(127,0xB)
                
                print ("****************************************")
                print ("FireFly number : ",x)
                print ("----------------------" )
                for reg in range(21) :
                    TimeLSB   = self.i2c_FireFly.read(128 + (reg * 3)) 
                    TimeMDL   = self.i2c_FireFly.read(129 + (reg * 3))
                    TimeMSB   = self.i2c_FireFly.read(130 + (reg * 3))
                    Time      = (TimeMSB * 0x10000 + TimeMDL * 0x100 + TimeLSB ) * 5
                    print (self.text_time[reg]% Time)
                
                # Peak temp
                temp = self.i2c_FireFly.read(194)
                print ("Peak temp -----:", temp)

            else:
                print ("FF : ",x, "not present" )

            FF_present = FF_present >> 4

