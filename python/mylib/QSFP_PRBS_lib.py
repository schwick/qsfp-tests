#!/usr/bin/python
import sys
import os
import select
import time
import hal
from mylib.Utility_lib import input2

__version__ = "01.00.00"

ADDRESSTABLE_NAME = 'DTHAddressMap.dat'

class QSFP_PRBS( hal.AXIDevice     ):
    '''Comment
    '''

    def __init__( self, deviceName, adressTablePath):
        '''The constructor of the self class



        '''
        # define the number of retry in a while
        self.MAX_ALLOW_RETRY    = 10

    ## ################################################################
    #Setup PRBS on QSFP
    def QSFP_PRBS_on(self,DAQ_module_offset):

        #(UG578 p155-p239)
        #Setup
        ########### setup PRBS Sender//receiver
        # TXPCSreset
        Reset_MAC = self.read64("mac100g_serdes_reset",DAQ_module_offset)
        Reset_MAC = Reset_MAC | 0x8
        self.write64("reset_hw_comp",Reset_MAC,False,0)

        # Change TXGEARBOX EN to 0  and TXBUF en 1
        #read 0x7c  TXGEARBOX_en (UG578 p435)    read  ONE CHANNEL
        self.write64("mac100g_serdes_chdrp_cmd",0x147C0000,False,DAQ_module_offset)

        # Check that the I2C access is done
        check = 0
        retry = 0
        while ( check != 0x1 and retry < self.MAX_ALLOW_RETRY):
            check     = (self.read64("mac100g_serdes_chdrp_st",DAQ_module_offset) ) & 0x1
##            print(hex(check))
            retry += 1

        if (retry == self.MAX_ALLOW_RETRY):
            print("Problem during DRP access when trying to switch on PRBS test !!")
            done = input("Hit return to continu")

        Value_DRP     = self.read64("mac100g_serdes_chdrp_dt",DAQ_module_offset)
        print ("DRP reg 0x7c value", hex(Value_DRP) )
        Value_DRP = 0xF87C0000 + (Value_DRP & 0xDFFF)     # TXGEARBOX <= '0'  bit 13
        Value_DRP = Value_DRP | 0x80                      # TCBUF_EN  <= '1'  bit 7

        self.write64("mac100g_serdes_chdrp_cmd",Value_DRP,False,DAQ_module_offset)

        # check that access was done successfully
        check = 0
        retry = 0
        while ( check != 0x1 and retry < self.MAX_ALLOW_RETRY):
            check     = (self.read64("mac100g_serdes_chdrp_st",DAQ_module_offset) ) & 0x1
##            print(hex(check))
            retry += 1

        if (retry == self.MAX_ALLOW_RETRY):
            print("Problem during DRP access when switching on PRBS test !!")
            done = input("Hit return to continu")

        #TXOUTCLKSEL <= 0x010    Loopback = 0   RXOUTCLKSEL <= 0x010
        self.write64("mac100g_serdes_loopback",0x220,False,DAQ_module_offset)

        #Select PRBS-31
        self.write64("mac100g_serdes_prbs_test",0x55,False,DAQ_module_offset)

        #release the reset
        Reset_MAC    = Reset_MAC & 0xFFFFFF7
        self.write64("mac100g_serdes_reset",Reset_MAC,False,DAQ_module_offset)

        check_TXRESET_DONE = 0
        retry = 0
        while(check_TXRESET_DONE != 0xF00 and retry < self.MAX_ALLOW_RETRY):
            TXreset_done    = self.read64("mac100g_serdes_prbs_test",DAQ_module_offset)
##            print("TX reset_done b11 : ",hex(TXreset_done))
            check_TXRESET_DONE = TXreset_done & 0xF00
            retry += 1

        if (retry == self.MAX_ALLOW_RETRY):
            print("Reset sequence problem !!")
            done = input("Hit return to continu")

    ## ################################################################
    # Disable PRBS on QSFP
    def QSFP_PRBS_off(self,DAQ_module_offset):

        ########### remove PRBS Sender//receiver
        # TXPCSreset
        Reset_MAC = self.read64("mac100g_serdes_reset",DAQ_module_offset)
        Reset_MAC = Reset_MAC | 0x8
        self.write64("reset_hw_comp",Reset_MAC,False,0)

        # Change TXGEARBOX EN to 0  and TXBUF en 1
        #read 0x7c  TXGEARBOX_en (UG578 p435)    read  ONE CHANNEL
        self.write64("mac100g_serdes_chdrp_cmd",0x147C0000,False,DAQ_module_offset)

        # Check that the I2C access is done
        check = 0
        retry = 0
        while ( check != 0x1 and retry < self.MAX_ALLOW_RETRY):
            check     = (self.read64("mac100g_serdes_chdrp_st",DAQ_module_offset) & 0x1)
            retry += 1

        if (retry == self.MAX_ALLOW_RETRY and retry < self.MAX_ALLOW_RETRY):
            print("Problem DRP access !!")
            done = input("Hit return to continu")

        Value_DRP     = self.read64("mac100g_serdes_chdrp_dt",DAQ_module_offset)
        print ("DRP reg 0x7c value", hex(Value_DRP) )
        Value_DRP = 0xF87C0000 + ((0xFFFF&Value_DRP) | 0x2000)     # TXGEARBOX <= '1'  bit 13
        Value_DRP = Value_DRP & 0xFFF7                    # TCBUF_EN  <= '0'  bit 7

        self.write64("mac100g_serdes_chdrp_cmd",Value_DRP,False,DAQ_module_offset)

        # Check that the I2C access is done
        check = 0
        retry = 0
        while ( check != 0x1 and retry < self.MAX_ALLOW_RETRY):
            check     = (self.read64("mac100g_serdes_chdrp_st",DAQ_module_offset) & 0x1)
            retry += 1

        if (retry == self.MAX_ALLOW_RETRY and retry < self.MAX_ALLOW_RETRY):
            print("Problem DRP access !!")
            done = input("Hit return to continu")

        #TXOUTCLKSEL <= 0x010    Loopback = 0   RXOUTCLKSEL <= 0x010
        self.write64("mac100g_serdes_loopback",0x250,False,DAQ_module_offset)

        #Select PRBS-31
        self.write64("mac100g_serdes_prbs_test",0x00,False,DAQ_module_offset)

        #release the reset
        Reset_MAC    = Reset_MAC & 0xFFFFFF7
        self.write64("mac100g_serdes_reset",Reset_MAC,False,DAQ_module_offset)

        check_TXRESET_DONE = 0
        retry = 0
        while(check_TXRESET_DONE != 0xF00 and retry < self.MAX_ALLOW_RETRY):
            TXreset_done    = self.read64("mac100g_serdes_prbs_test",DAQ_module_offset)
##            print("TX reset_done b11 : ",hex(TXreset_done))
            check_TXRESET_DONE = TXreset_done & 0xF00
            retry += 1

        if (retry == self.MAX_ALLOW_RETRY):
            print("Reset sequence problem !!")
            done = input("Hit return to continue")

     ##################################################################
    #Reset the PRBS counters
    def Reset_PRBS(self,DAQ_module_offset):


        #Reset PRBS counter
        Reset_val = self.read64("mac100g_serdes_prbs_test",DAQ_module_offset)
        Reset_PRBS = Reset_val | 0x10000000
        self.write64("mac100g_serdes_prbs_test",Reset_PRBS,False,DAQ_module_offset)
        self.write64("mac100g_serdes_prbs_test",Reset_val,False,DAQ_module_offset)

    ##################################################################
    def PRBS_locked(self,QSFP,DAQ_module_offset, display=1):

        #Reset PRBS counter
        PRBS_locked = self.read64("mac100g_serdes_prbs_test",DAQ_module_offset)
        PRBS_locked = (PRBS_locked & 0xF000000) >> 24
        if display:
            print("DAQ link : ",QSFP," locked if not null : ",hex(PRBS_locked))
        return PRBS_locked
    
    ##################################################################
    def Read_PRBS_results_on_time(self,QSFP,display,DAQ_module_offset):
        
        result = [0] * 4
        #Reset PRBS err counter
        self.write64("mac100g_serdes_chdrp_cmd",0xF65E0000,False,DAQ_module_offset)
        check = 0
        retry = 0
        while (check != 0xF):
            check     = self.read64("mac100g_serdes_chdrp_st",DAQ_module_offset) & 0xF
            retry += 1

        if (retry == self.MAX_ALLOW_RETRY):
            print("Problem DRP access !!")
            done = input("Hit return to continu")

        Value_DRP     = self.read64("mac100g_serdes_chdrp_dt",DAQ_module_offset)
        Link0_low    = (Value_DRP & 0xFFFF)
        Link1_low    = (Value_DRP & 0xFFFF0000) >> 16
        Link2_low    = (Value_DRP & 0xFFFF00000000) >> 32
        Link3_low    = (Value_DRP & 0xFFFF000000000000) >> 48

        self.write64("mac100g_serdes_chdrp_cmd",0xF65F0000,False,DAQ_module_offset)
        check = 0
        retry = 0
        while (check != 0xF):
            check     = self.read64("mac100g_serdes_chdrp_st",DAQ_module_offset) & 0xF
            retry += 1

        if (retry == self.MAX_ALLOW_RETRY):
            print("Problem DRP access !!")
            done = input("Hit return to continu")

        Value_DRP     = self.read64("mac100g_serdes_chdrp_dt",DAQ_module_offset)
        Link0_high    = (Value_DRP & 0xFFFF)
        Link1_high    = (Value_DRP & 0xFFFF0000) >> 16
        Link2_high    = (Value_DRP & 0xFFFF00000000) >> 32
        Link3_high    = (Value_DRP & 0xFFFF000000000000) >> 48

        Link0        = Link0_high * 0x10000 + Link0_low
        Link1        = Link1_high * 0x10000 + Link1_low
        Link2        = Link2_high * 0x10000 + Link2_low
        Link3        = Link3_high * 0x10000 + Link3_low

        if display:
            print("*****************************")
            print("PRBS result DAQ : ",QSFP)
            print("Err link 0 : ", Link0)
            print("Err link 1 : ", Link1)
            print("Err link 2 : ", Link2)
            print("Err link 3 : ", Link3)

        result[0] = Link0
        result[1] = Link1
        result[2] = Link2
        result[3] = Link3

        return result

    ##################################################################
    def PRBS_generate_error(self,DAQ_module_offset):

        #Generate PRBS error  #(UG578 p155-p239)
        Reset_val = self.read64("mac100g_serdes_prbs_test",DAQ_module_offset)
        Reset_PRBS = Reset_val | 0x80000000
        self.write64("mac100g_serdes_prbs_test",Reset_PRBS,False,DAQ_module_offset)


    def setLinkParams( self, DAQ_module_offset, line_mask,params, ena_DFE = 1 ):
        ''' params is a dictionary which may have any of the following keys:
        diff_v, pre, post, ena_DFE
        The parameters can be set individually for the 4 25GBps lines. The
        line mask contains a 4 bit mask for the 4 lines. If a bit is at 0
        the line values are not changed (i.e. the values are read out and
        re-written at the same position). '''

        old = self.read64("mac100g_serdes_voltage",DAQ_module_offset)
        new = old
        for i in range(0,4):
            line = 1<<i
            if line & line_mask:
                # we need to change this line
                line_bits = (0x7FFF << (i*16))
                new_bits = 0
                if "diff_v" in params:
                    new_bits = params['diff_v']
                if "pre" in params:
                    new_bits = new_bits | (params['pre'] << 5 )
                if "post" in params:
                    new_bits = new_bits | (params['post'] << 10 )

                res_bits = ~line_bits
                #print( "res mask %0x" % res_bits )
                new = new & res_bits
                new = new | (new_bits << (i*16))
        if ena_DFE :
            new = new | (1<<63)
        else:
            new = new & 0x7FFFFFFFFFFFFFFF
            
        #print ("old analogue settings: 0x%016x" % old)
        #print ("new analogue settings: 0x%016x" % new)

        self.write64("mac100g_serdes_voltage",new,False,DAQ_module_offset)
        
    ##################################################################
    def Change_analog(self,DAQ_module_offset):
        ###
        ###   A REVOIR
        ###
        # Actually all links are set identically
        for x in range(0,1):
            #read back
            val = self.read64("mac100g_serdes_voltage",DAQ_module_offset)
            #print("analog values : " , hex(val))
            TXdiff_rd         = (val & 0x1F         )
            post_cursor_rd     = (val & 0x3E0         )>>5
            pre_cursor_rd     = (val & 0x7C00     )>>10
            tx_polarity_rd     = (val & 0x8000     )>> 15
            rx_polarity_rd     = (val & 0x80000000    )>> 31
            DFE_ena_rd         = (val & 0x8000000000000000 )>> 63
            
        TXdiff             = input2("Diff Swing value?  (dec)", TXdiff_rd)
        post_cursor        = input2("PostCursor? (dec)", post_cursor_rd)
        pre_cursor        = input2("PreCursor?  (dec)", pre_cursor_rd)
        tx_polarity     = input2("1: change polarity Tx, 0:no", tx_polarity_rd)
        rx_polarity     = input2("1: change polarity Rx, 0:no", rx_polarity_rd)
        DFE_ena            = input2("1: LPM    0:DFE (PRBS)", DFE_ena_rd)

        for x in range(0,5):
            #value = hex(rx_polarity)
            interm    = (pre_cursor * 0x400) | (post_cursor * 0x20) | TXdiff
            value = (int(DFE_ena) * 0x8000000000000000) | (int(rx_polarity) * 0x80000000) | (int(tx_polarity) * 0x8000) | (interm * 0x1000000000000) |(interm * 0x100000000) | (interm * 0x10000) | (interm * 0x1)
            print ("value written : ", hex(value))
            self.write64("mac100g_serdes_voltage",value,False,DAQ_module_offset)

