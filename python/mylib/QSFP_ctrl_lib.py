#!/usr/bin/python
import sys
import os
import select
import time
import hal
from I2CInterface import I2CInterface
from threading import Lock

__version__ = "01.00.00"

ADDRESSTABLE_NAME = 'DTHAddressMap.dat'

class QSFP_Ctrl( hal.AXIDevice ):
    '''Comment
    '''

    def __init__( self, deviceName, adressTablePath):
        '''The constructor of the DTH class


        '''
        self.lock = Lock()
        # Initialise the i2c interfaces of the dth
        self.i2c_switch  = I2CInterface(self,0xE0, offsetWidth = 0x0 , dataWidth = 0x1, debugFlag=0,
                                        items={'i2c_a': 'qsfp_i2c_access_a', 'i2c_b': 'qsfp_i2c_access_b','i2c_poll': 'qsfp_i2c_access_a_access_done'})
        self.i2c_QSFP = I2CInterface(self,0xA0, offsetWidth = 0x1 , dataWidth = 0x1,
                                        items={'i2c_a': 'qsfp_i2c_access_a', 'i2c_b': 'qsfp_i2c_access_b','i2c_poll': 'qsfp_i2c_access_a_access_done'})

        #  This parameter specifies the switch Output order for QSFP 0,1,2,3,4
        self.switch_TCA = [0x10,0x08,0x04,0x02,0x01]
        #self.switch_TCA = [0x01,0x02,0x04,0x08,0x10]

        self.QSFP_available = []
        print("check availability")
        for i in range (0,5):
            if self.QSFP_present( i ):
                self.QSFP_available.append(i)
                print( "QSFP in slot %i available" % i)


                
    def getInfo(self, i_qsfp ):

        self.lock.acquire()
        res = { 'slot_no' : i_qsfp }

        if i_qsfp not in self.QSFP_available:
            res[ 'qsfp_plugged' ] = False
            self.lock.release()
            return res
        res[ 'qsfp_plugged' ] = True

        self.i2c_switch.write(0x0,self.switch_TCA[i_qsfp])

        res['ID'] = self.i2c_QSFP.read(0)
        res['Revision'] = self.i2c_QSFP.read(1)
        res['status'] = self.i2c_QSFP.read(2)
        res['Tmp_alarm'] = self.i2c_QSFP.read(6)
        res['SupplyVoltage_alarm'] = self.i2c_QSFP.read(7)
        res['TransceiverRXPower_12_alarm'] = self.i2c_QSFP.read(9)
        res['TransceiverRXPower_34_alarm'] = self.i2c_QSFP.read(10)
        res['TransceiverBias_12_alarm'] = self.i2c_QSFP.read(11)
        res['TransceiverBias_34_alarm'] = self.i2c_QSFP.read(12)
        res['TransceiverTXPower_12_alarm'] = self.i2c_QSFP.read(13)
        res['TransceiverTXPower_34_alarm'] = self.i2c_QSFP.read(14)
        res['Temperature'] = ((self.i2c_QSFP.read(22) << 8) + self.i2c_QSFP.read(23)) / 256.
        res['SupplyVoltage'] = ((self.i2c_QSFP.read(26) << 8) + self.i2c_QSFP.read(27)) / 10000.
        for i in range(1,5):
            res['RX%dPower' % i] =  ((self.i2c_QSFP.read(32+i*2) << 8) + self.i2c_QSFP.read(33+i*2)) / 10000.  # mW
            res['TX%dPower' % i] =  ((self.i2c_QSFP.read(48+i*2) << 8) + self.i2c_QSFP.read(49+i*2)) / 10000.  # mW
            res['TX%dBiasCurrent' % i] =  ((self.i2c_QSFP.read(40+i*2) << 8) + self.i2c_QSFP.read(41+i*2)) * 0.002  # mA

        # select page 0
        self.i2c_QSFP.write( 0x7F, 0 )
        # if the following sleep is missing the AVAGO some times give wrong characters in the vendor.
        time.sleep(0.1)
        res['Identifier'] = self.i2c_QSFP.read(128)
        res['Extended Identifier'] = self.i2c_QSFP.read(129)

        ioff = 148
        vname = ""        
        c = self.i2c_QSFP.read(ioff)
        while (c != 0) & (ioff <= 163):
            vname = vname + chr(c)
            ioff += 1
            c = self.i2c_QSFP.read(ioff)
        res['Vendor Name'] = vname

        #print('vname', vname)
        
        ioff = 168
        vname = ""        
        c = self.i2c_QSFP.read(ioff)
        while (c != 0) & (ioff <= 183):
            vname = vname + chr(c)
            ioff += 1         
            c = self.i2c_QSFP.read(ioff)
        res['Vendor PN'] = vname

        ioff = 196
        vname = ""        
        c = self.i2c_QSFP.read(ioff)
        while (c != 0) & (ioff <= 211):
            vname = vname + chr(c)
            ioff += 1
            c = self.i2c_QSFP.read(ioff)
                    
        res['Vendor SN'] = vname

        res['RXEqualizerTXEmphasis'] = self.i2c_QSFP.read(193) # Capabilities for emphasis (SFF-8636 Tab 6-23)
        res['CDR Features'] = self.i2c_QSFP.read(194) # 7,6: tx/rx CDR on/off impl. 5,4: LOL impl. 0-3 squelch
        res['Rate Select Features'] = self.i2c_QSFP.read(195)
        res['Enhanced Options'] = self.i2c_QSFP.read(221) # bit 3='1' : rate selection enabled; bit2 : application rate sel
        res['CDR Control'] = self.i2c_QSFP.read(98)
        res['RX Rate Select'] = self.i2c_QSFP.read(87)
        res['TX Rate Select'] = self.i2c_QSFP.read(88)
        res['Nominal BR'] = self.i2c_QSFP.read(140)
        res['Nominal BR high'] = self.i2c_QSFP.read(222)
        self.lock.release()
        return res

    def readVendor( self, i_qsfp ):
        ioff = 148
        vname = ""        
        c = self.i2c_QSFP.read(ioff)
        while (c != 0) & (ioff <= 163):
            vname = vname + chr(c)
            ioff += 1
            c = self.i2c_QSFP.read(ioff)
        return vname

    def _dumpKeys( self, keys, info ):
        for key in keys:
            val = info[key]
            if isinstance(val, float ):
                print( "%30s : %8.2f" % (key,val))
            elif isinstance (val, str):
                print( "%30s : %s" % (key,val))
            else:
                print( "%30s : %8.2d  0x%02x" % (key,val,val))

       
    def dumpInfo( self, i_qsfp ):
        info = self.getInfo( i_qsfp )
        print("")
        print("")
        print( "QSFP SLOT %d" % i_qsfp )
        print( "============")
        print()
        
        if info['qsfp_plugged'] == 0:
            print( "No QSFP plugged in slot %d." % i_qsfp )
            return

        print("")
        print( "General Device Information")
        print( "--------------------------")
        keys = ["Vendor Name","Vendor PN","Vendor SN",
                "ID","Revision","Identifier","Extended Identifier"]
        self._dumpKeys( keys, info )

        print("")
        print( "Features")
        print( "--------")
        keys = ["CDR Features","CDR Control","Enhanced Options",
                "Rate Select Features","RXEqualizerTXEmphasis",
                "Nominal BR", "Nominal BR high"]
        self._dumpKeys(keys, info)
        
        print("")
        print( "Temperature and electrical status" )
        print( "---------------------------------" )
        keys = ["Temperature", "SupplyVoltage", "TX1Power",  "TX2Power",  "TX3Power",  "TX4Power",  "RX1Power", "RX2Power", "RX3Power", "RX4Power", 'TX1BiasCurrent', 'TX2BiasCurrent', 'TX3BiasCurrent', 'TX4BiasCurrent']
    
        self._dumpKeys(keys, info)

        print("")
        print( "Alarms")
        print( "------")
        keys = ["SupplyVoltage_alarm","Tmp_alarm",
                "TransceiverRXPower_12_alarm","TransceiverRXPower_34_alarm",
                "TransceiverTXPower_12_alarm","TransceiverTXPower_34_alarm",
                "TransceiverBias_12_alarm","TransceiverBias_34_alarm"]
        self._dumpKeys( keys, info )
    
        print("")
        print("Status information")
        print("------------------")
        keys = ["status"]
        self._dumpKeys(keys,info)

    ##################################################################
    # enable QSFP
    def Enable_QSFP(self):
        '''By default the QSFPs are enabled. They are also not reset with
        the general hardware reset (since the switch on the other side of
        the link takes a long time to recognise the link when it comes up
        again.'''
        
        print("Enable QSFP")

        # realise RESET on QSFP28
        ena_qsfp = 0x0202020202
        self.write64("qsfp_monitoring",ena_qsfp)

        #read QSFP status
        value_read =  self.read64("qsfp_monitoring",0)
        value_read = hex(int(value_read))
        print ("Firefly status : (_CS,_Reset,_Present,Int,LPower(8b))" , value_read)
        

    
    ##################################################################
    # check if a qsfp is present in slot "qsfp_no". If so, return True
    def QSFP_present( self, qsfp_no ):
        print("present %d"%qsfp_no)
        value =  self.read64("qsfp_monitoring",0)
        
        return ( ( ( value >> (qsfp_no * 8) ) & 0x04) == 0x00 )
        
    ##################################################################
    # Temperature QSFP
    def Temp_QSFP(self):

        # read if QSFP is present?
        QSFP_present =  self.read64("qsfp_monitoring",0)

        for x in range(0,5):            
            if (QSFP_present & 0x4 == 0):
                print ("------ Access QSFP no : " , str(x) , "------")
                # access the TCA9548 for clock control
                self.i2c_switch.write(0x0,self.switch_TCA[x])

                Temp_MSB = self.i2c_QSFP.read(22)
                Temp_LSB = self.i2c_QSFP.read(23)
                Temperature = (Temp_LSB + (Temp_MSB * 0x100)) / 256
                print ("QSFP no : ",x," Temp : ",round(Temperature,2)," deg")

            QSFP_present = QSFP_present >> 8

    ##################################################################
    # Read Rx power QSFP
    def RxPower_QSFP(self):

        for x in range(0,5):
            try:
                if self.QSFP_present( x ):
                    print ("------ Access QSFP no : " , str(x) , "------")
                    # access the TCA9548 for clock control
                    self.i2c_switch.write(0x0,self.switch_TCA[x])
                                
                    RX_rate     = self.i2c_QSFP.read(87)
                    TX_rate     = self.i2c_QSFP.read(88)
                    if (RX_rate == 0xAA and TX_rate == 0xAA):
                        print("QSFP 4x 25Gb/s")
                    else :
                        print("QSFP haven't the correct rate, change it! TX_rate %d   RX_rate %d" % (RX_rate, TX_rate))
                
                    #set the Page 3
                    #print("set page 3")
                    #try:
                    #    self.i2c_QSFP.write(0x7F,3)
                    #except Exception as e:
                    #    print ("Did not work: ",e)

                    
                    for link in range(0,4):
                        RX_MSB = self.i2c_QSFP.read(34+(2*link))
                        RX_LSB = self.i2c_QSFP.read(35+(2*link))
                        rx_power = (RX_LSB + (RX_MSB * 0x100)) * 0.1
                        print ("Lane : ",link," RX power :",round(rx_power,1)," uW")
                
                    for link in range(0,4):
                        TX_MSB = self.i2c_QSFP.read(50+(2*link))
                        TX_LSB = self.i2c_QSFP.read(51+(2*link))
                        tx_power = (TX_LSB + (TX_MSB * 0x100)) * 0.1
                        print ("Lane : ",link," TX power :",round(tx_power,1)," uW")
            except Exception as e:
                print( "Exception when reading Rx/Tx Power of QSFP %x : %s" % (x,str(e) ) )  


 
