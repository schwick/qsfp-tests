#!/usr/bin/python
import sys
import os
import select
import time
import hal
from mylib.Utility_lib import input2

__version__ = "01.00.00"

ADDRESSTABLE_NAME = 'DTHAddressMap.dat'

class SRr_PRBS( hal.AXIDevice     ):
    '''Comment
    '''

    def __init__( self, deviceName, adressTablePath):
        '''The constructor of the self class


        '''

        # define the number of retry in a while
        self.MAX_ALLOW_RETRY                   = 10

    ##################################################################
    #Setup PRBS on FIREFLY
    def SR_PRBS_on(self,SR_module_offset):


        #(UG578 p155-p239)

        ########### setup PRBS Sender//receiver
        # TXPCSreset
        Reset_MAC = self.read64("slinkrocket_rcv_serdes_control",SR_module_offset)
        Reset_MAC = Reset_MAC | 0x4
        self.write64("reset_hw_comp",Reset_MAC,False,0)

        # Change TXGEARBOX EN to 0  and TXBUF en 1
        #read 0x7c  TXGEARBOX_en (UG578 p435)    read  ONE CHANNEL
        self.write64("slinkrocket_rcv_serdes_drp_control",0x807C0000,False,SR_module_offset)

        # Check that the I2C access is done
        check = 0
        retry = 0
        while ( check == 0x0 and retry < self.MAX_ALLOW_RETRY):
            check   = (self.read64("slinkrocket_rcv_serdes_drp_data_returned",SR_module_offset) )
##            print ("DRP1 read done : ",hex(check))
            check = check & 0x10000
            retry += 1

        if (retry == self.MAX_ALLOW_RETRY):
            print("Problem DRP access !!")
            done = input("Hit return to continu")

        Value_DRP   = self.read64("slinkrocket_rcv_serdes_drp_data_returned",SR_module_offset)
##        print ("DRP reg 0x7c value", hex(Value_DRP & 0xFFFF) )

        Value_DRP = 0x407C0000 + (Value_DRP & 0xDFFF)   # TXBEARBOX <= '0'  bit 13
        Value_DRP = Value_DRP | 0x80                    # TCBUF_EN  <= '1'  bit 7
        self.write64("slinkrocket_rcv_serdes_drp_control",Value_DRP,False,SR_module_offset)

        # Check that the I2C access is done
        check = 0
        retry = 0
        while ( check == 0x0 and retry < self.MAX_ALLOW_RETRY):
            check   = (self.read64("slinkrocket_rcv_serdes_drp_data_returned",SR_module_offset) )
##            print ("DRP2 read done : ",hex(check))
            check = check & 0x10000
            retry += 1

        if (retry == self.MAX_ALLOW_RETRY):
            print("Problem DRP access !!")
            done = input("Hit return to continu")


        #TXOUTCLKSEL <= 0x010   Loopback = 0   RXOUTCLKSEL <= 0x010
        self.write64("slinkrocket_rcv_serdes_loopback",0x00000002,False,SR_module_offset)

        #Select PRBS-31 for TX and RX
        self.write64("slinkrocket_serdes_prbs_test",0x55,False,SR_module_offset)

        #release the reset
        Reset_MAC   = Reset_MAC & 0xFFFFFFB
        self.write64("slinkrocket_rcv_serdes_control",Reset_MAC,False,SR_module_offset)

        check_TXRESET_DONE = 0
        retry = 0
        while(check_TXRESET_DONE == 0 and retry < self.MAX_ALLOW_RETRY):
            TXreset_done    = self.read64("slinkrocket_rcv_serdes_loopback",SR_module_offset)
##            print("TX reset_done b11 : ",hex(TXreset_done))
            check_TXRESET_DONE = TXreset_done & 0x800
            retry += 1

        if (retry == self.MAX_ALLOW_RETRY):
            print("Reset sequence problem !!")
            done = input("Hit return to continu")


        #execute a PRBS reset counter (For RX link )

        #Reset PRBS counter
        Reset_val = self.read64("slinkrocket_serdes_prbs_test",SR_module_offset)
        Reset_PRBS = Reset_val | 0x10000000
        self.write64("slinkrocket_serdes_prbs_test",Reset_PRBS,False,SR_module_offset)
        Reset_PRBS = Reset_val & 0xEFFFFFFF
        self.write64("slinkrocket_serdes_prbs_test",Reset_val,False,SR_module_offset)

    ##################################################################
    #Disable PRBS on FIREFLY
    def SR_PRBS_off(self,SR_module_offset):
        #(UG578 p155-p239)

        ########### setup PRBS Sender//receiver
        # TXPCSreset
        Reset_MAC = self.read64("slinkrocket_rcv_serdes_control",SR_module_offset)
        Reset_MAC = Reset_MAC | 0x4
        self.write64("reset_hw_comp",Reset_MAC,False,0)

        # Change TXGEARBOX EN to 0  and TXBUF en 1
        #read 0x7c  TXGEARBOX_en (UG578 p435)    read  ONE CHANNEL
        self.write64("slinkrocket_rcv_serdes_drp_control",0x807C0000,False,SR_module_offset)

        # Check that the I2C access is done
        check = 0
        retry = 0
        while ( check == 0x0 and retry < self.MAX_ALLOW_RETRY):
            check   = (self.read64("slinkrocket_rcv_serdes_drp_data_returned",SR_module_offset) )
##            print ("DRP read done : ",hex(check))
            check = check & 0x10000
            retry += 1

        if (retry == self.MAX_ALLOW_RETRY):
            print("Problem DRP access !!")
            done = input("Hit return to continu")

        Value_DRP   = self.read64("slinkrocket_rcv_serdes_drp_data_returned",SR_module_offset)
##        print ("DRP reg 0x7c value", hex(Value_DRP & 0xFFFF) )
        Value_DRP = 0x407C0000 + ((Value_DRP & 0xFFFF) | 0x2000)    # TXBEARBOX <= '1'  bit 13
        Value_DRP = Value_DRP & 0xFFFFFF7F                          # TCBUF_EN  <= '0'  bit 7

        self.write64("slinkrocket_rcv_serdes_drp_control",Value_DRP,False,SR_module_offset)

        # Check that the I2C access is done
        check = 0
        retry = 0
        while ( check == 0x0 and retry < self.MAX_ALLOW_RETRY):
            check   = (self.read64("slinkrocket_rcv_serdes_drp_data_returned",SR_module_offset) )
##            print ("DRP2 read done : ",hex(check))
            check = check & 0x10000
            retry += 1

        if (retry == self.MAX_ALLOW_RETRY):
            print("Problem DRP access !!")
            done = input("Hit return to continu")

        #TXOUTCLKSEL <= 0x010   Loopback = 0   RXOUTCLKSEL <= 0x010
        self.write64("slinkrocket_rcv_serdes_loopback",0x005,False,SR_module_offset)

        #remove PRBS-31 for TX and RX
        self.write64("slinkrocket_serdes_prbs_test",0x00,False,SR_module_offset)

        #release the reset
        Reset_MAC   = Reset_MAC & 0xFFFFFFB
        self.write64("slinkrocket_rcv_serdes_control",Reset_MAC,False,SR_module_offset)


        check_TXRESET_DONE = 0
        retry = 0
        while(check_TXRESET_DONE == 0 and retry < self.MAX_ALLOW_RETRY):
            TXreset_done    = self.read64("slinkrocket_rcv_serdes_loopback",SR_module_offset)
##            print("TX reset_done b11 : ",hex(TXreset_done))
            check_TXRESET_DONE = TXreset_done & 0x800
            retry += 1

        if (retry == self.MAX_ALLOW_RETRY):
            print("Reset sequence problem !!")
            done = input("Hit return to continu")

    ##################################################################
    #Reset the PRBS counters
    def Reset_PRBS(self,SR_module_offset):

        #Reset PRBS counter
        Reset_val = self.read64("slinkrocket_serdes_prbs_test",SR_module_offset)
        Reset_PRBS = Reset_val | 0x10000000
        self.write64("slinkrocket_serdes_prbs_test",Reset_PRBS,False,SR_module_offset)

        Reset_PRBS = Reset_val & 0xEFFFFFFF
        self.write64("slinkrocket_serdes_prbs_test",Reset_val,False,SR_module_offset)

    ##################################################################
    def PRBS_locked(self,SR_module_offset):

        #Reset PRBS counter
        PRBS_locked = self.read64("slinkrocket_serdes_prbs_test",SR_module_offset)
        #print (hex(PRBS_locked))
        PRBS_locked = (PRBS_locked & 0x1000000) >> 24
        print("=>  locked if not nul : ",hex(PRBS_locked))

    ##################################################################
    def Read_PRBS_results_on_time(self,link,display,SR_module_offset):
        result = 0
        #read PRBS err counter
        self.write64("slinkrocket_rcv_serdes_drp_control",0x825E0000,False,SR_module_offset)

        check = 0
        retry = 0
        while (check == 0x0 and retry < self.MAX_ALLOW_RETRY):
            check   = self.read64("slinkrocket_rcv_serdes_drp_data_returned",SR_module_offset) & 0x10000
            retry += 1

        if (retry == self.MAX_ALLOW_RETRY):
            print("Problem DRP access !!")
            done = input("Hit return to continu")

        Value_DRP   = self.read64("slinkrocket_rcv_serdes_drp_data_returned",SR_module_offset)
        Link0_low   = (Value_DRP & 0xFFFF)

        self.write64("slinkrocket_rcv_serdes_drp_control",0x825F0000,False,SR_module_offset)

        check = 0
        retry = 0
        while (check == 0x0 and retry < self.MAX_ALLOW_RETRY):
            check   = self.read64("slinkrocket_rcv_serdes_drp_data_returned",SR_module_offset) & 0x10000
            retry += 1

        if (retry == self.MAX_ALLOW_RETRY):
            print("Problem DRP access !!")
            done = input("Hit return to continu")

        Value_DRP   = self.read64("slinkrocket_rcv_serdes_drp_data_returned",SR_module_offset)
        Link0_high  = (Value_DRP & 0xFFFF)

        Link0       = Link0_high * 0x10000 + Link0_low

        if display:
            #print("*****************************")
            print("PRBS result FireFly : ",link, " Err link : ", Link0)

        result = Link0

        return result

    ##################################################################
    def PRBS_generate_error(self,SR_module_offset):

        #Generate PRBS error  #(UG578 p155-p239)
        Reset_val = self.read64("slinkrocket_serdes_prbs_test",SR_module_offset)
        Reset_PRBS = Reset_val | 0x80000000
        self.write64("slinkrocket_serdes_prbs_test",Reset_PRBS,False,SR_module_offset)


    ##################################################################
    def Change_analog(self,SR_module_offset):
        ###
        ###   A REVOIR
        ###
        # First read the previous configuration of the First FF (Actually all are set with same values)
        val = self.read64("slinkrocket_rcv_serdes_analog",SR_module_offset )
        #print("analog values : " , hex(val))
        TXdiff_rd       = (val & 0x1F       )
        post_cursor_rd  = (val & 0x1F00     )>>8
        pre_cursor_rd   = (val & 0x1F0000   )>>16

        #Input new values
        TXdiff          = input2("Diff Swing value?  (dec)", TXdiff_rd)
        post_cursor     = input2("PostCursor? (dec)", post_cursor_rd)
        pre_cursor      = input2("PreCursor?  (dec)", pre_cursor_rd)

        #value = hex(rx_polarity)
        value   = (pre_cursor * 0x10000) | (post_cursor * 0x100) | TXdiff
        print ("value written : ", hex(value))
        self.write64("slinkrocket_rcv_serdes_analog",value,False,SR_module_offset )