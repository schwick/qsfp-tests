#!/usr/bin/python
import sys
import os
import select
import time
import hal

__version__ = "01.00.00"

#ADDRESSTABLE_NAME = 'selfAddressMap.dat'

class TCDS2_PRBS( hal.AXIDevice     ):
    '''Comment
    '''

    def __init__( self, deviceName, adressTablePath):
        '''The constructor of the self class



        '''
        # define the number of retry in a while
        self.MAX_ALLOW_RETRY    = 10

    ## ################################################################
    #Setup PRBS on QSFP
    def QSFP_PRBS_on(self,DRP_ena_per_module,TCDS_PRBS_module_offset):

        #(UG578 p155-p239)
        #Setup
        ########### setup PRBS Sender//receiver
        # TXPCSreset  + Disable Checked reset Logic
        Reset_MAC =  0x1100000
        self.write64("TCDS_SERDES_reset",Reset_MAC,False,TCDS_PRBS_module_offset)

        # Change TXGEARBOX EN to 0  and TXBUF en 1
        #read 0x7c  TXGEARBOX_en (UG578 p435)    read  ONE CHANNEL
        self.write64("TCDS_SERDES_ChDRP_cmd",0xF47C0000,False,TCDS_PRBS_module_offset)
        check     = 0
        nb_retry = 0
        while ( check != (DRP_ena_per_module)) and nb_retry <= self.MAX_ALLOW_RETRY:
            check     = (self.read64("TCDS_SERDES_ChDRP_st",TCDS_PRBS_module_offset) ) & 0xF
            #print ("DRP1 read done : ",hex(check))
            nb_retry +=1

        print(nb_retry)
        if nb_retry == self.MAX_ALLOW_RETRY:
            print("Something should be checked, becasue the previous access was unsuccessful !!  ")

        Value_DRP     = self.read64("TCDS_SERDES_ChDRP_dt",TCDS_PRBS_module_offset)
        print("DRP reg 0x7c value before change : ", hex(Value_DRP & 0xFFFF) )


        Value_DRP = 0xF87C0000 + (Value_DRP & 0xDFFF)     # TXBEARBOX <= '0'  bit 13
        Value_DRP = Value_DRP | 0x80                    # TCBUF_EN  <= '1'  bit 7
        self.write64("TCDS_SERDES_ChDRP_cmd",Value_DRP,False,TCDS_PRBS_module_offset)

        #read back value set
        self.write64("TCDS_SERDES_ChDRP_cmd",0xF47C0000,False,TCDS_PRBS_module_offset)
        check = 0
        nb_retry = 0
        while (check != (DRP_ena_per_module)) and nb_retry <= self.MAX_ALLOW_RETRY:
            check     = (self.read64("TCDS_SERDES_ChDRP_st",TCDS_PRBS_module_offset) ) & 0xF
            #print ("DRP2 read done : ",hex(check))
            nb_retry +=1

        print(nb_retry)
        if nb_retry == self.MAX_ALLOW_RETRY:
            print("Something should be checked, becasue the previous access was unsuccessful !!  ")

        Value_DRP     = self.read64("TCDS_SERDES_ChDRP_dt",TCDS_PRBS_module_offset)
        print("DRP reg x7C after change : ",hex(Value_DRP))


        #TXOUTCLKSEL <= 0x010
        self.write64("TCDS_SERDES_PRBS_TEST",0x200000,False,TCDS_PRBS_module_offset)

        #Select PRBS-31 for TX and RX
        self.write64("TCDS_SERDES_PRBS_TEST",0x200055,False,TCDS_PRBS_module_offset)

        #release the reset
        self.write64("TCDS_SERDES_reset",0x0100000,False,TCDS_PRBS_module_offset)

        # Check TXresetDONE = 1
        check_TXRESET_DONE = 0
        nb_retry = 0
        while (check_TXRESET_DONE == 0) and nb_retry <= self.MAX_ALLOW_RETRY:
            #print("Wait TXRESET_DONE!",hex(check_TXRESET_DONE))
            check_TXRESET_DONE = self.read64("TCDS_SERDES_reset",TCDS_PRBS_module_offset) & 0x40000000
            nb_retry +=1

        if nb_retry == self.MAX_ALLOW_RETRY:
            print("Something should be checked, becasue the previous access was unsuccessful !!  ")


        # Reset PRBS counter
        Reset_val = self.read64("TCDS_SERDES_PRBS_TEST",TCDS_PRBS_module_offset)
        Reset_PRBS = Reset_val | 0x10000000
        print("TCDS_SERDES_PRBS_TEST : ",hex(Reset_PRBS))
        self.write64("TCDS_SERDES_PRBS_TEST",Reset_PRBS,False,TCDS_PRBS_module_offset)
        Reset_PRBS    = Reset_PRBS & 0xFFFFFF
        self.write64("TCDS_SERDES_PRBS_TEST",Reset_PRBS,False,TCDS_PRBS_module_offset)


    ## ################################################################
    # Disable PRBS on QSFP
    def QSFP_PRBS_off(self,DRP_ena_per_module,TCDS_PRBS_module_offset):

        ########### setup PRBS Sender//receiver
        # TXPCSreset
        Reset_MAC =  0x1000000
        self.write64("TCDS_SERDES_reset",Reset_MAC,False,TCDS_PRBS_module_offset)

        # Change TXGEARBOX EN to 0  and TXBUF en 1
        #read 0x7c  TXGEARBOX_en (UG578 p435)    read  ONE CHANNEL
        self.write64("TCDS_SERDES_ChDRP_cmd",0xF47C0000,False,TCDS_PRBS_module_offset)
        check = 0
        nb_retry = 0
        while ( check != (DRP_ena_per_module)) and nb_retry <= self.MAX_ALLOW_RETRY:
            check     = (self.read64("TCDS_SERDES_ChDRP_st",TCDS_PRBS_module_offset) ) & 0xF
            #print ("DRP1 read done : ",hex(check))
            nb_retry +=1

        if nb_retry == self.MAX_ALLOW_RETRY:
            print("Something should be checked, becasue the previous access was unsuccessful !!  ")

        Value_DRP     = self.read64("TCDS_SERDES_ChDRP_dt",TCDS_PRBS_module_offset)
        print ("DRP reg 0x7c value before : ", hex(Value_DRP & 0xFFFF) )

        Value_DRP = 0xF87C0000 + ((Value_DRP & 0xFFFF) | 0x2000)     # TXBEARBOX <= '1'  bit 13
        Value_DRP = Value_DRP & 0xFFFFFF7F                           # TCBUF_EN  <= '0'  bit 7
        self.write64("TCDS_SERDES_ChDRP_cmd",Value_DRP,False,TCDS_PRBS_module_offset)

        #read back value set
        self.write64("TCDS_SERDES_ChDRP_cmd",0xF47C0000,False,TCDS_PRBS_module_offset)
        check = 0
        nb_retry = 0
        while (check != (DRP_ena_per_module)) and nb_retry <= self.MAX_ALLOW_RETRY:
            check     = (self.read64("TCDS_SERDES_ChDRP_st",TCDS_PRBS_module_offset) ) & 0xF
            #print ("DRP2 read done : ",hex(check))
            nb_retry +=1

        if nb_retry == self.MAX_ALLOW_RETRY:
            print("Something should be checked, becasue the previous access was unsuccessful !!  ")

        Value_DRP     = self.read64("TCDS_SERDES_ChDRP_dt",TCDS_PRBS_module_offset)
        print("DRP reg x7C after change : ",hex(Value_DRP))

        #TXOUTCLKSEL <= 0x101
        self.write64("TCDS_SERDES_PRBS_TEST",0x500055,False,TCDS_PRBS_module_offset)

        #Select PRBS-31 for TX and RX
        self.write64("TCDS_SERDES_PRBS_TEST",0x500000,False,TCDS_PRBS_module_offset)

        #release the reset
        self.write64("TCDS_SERDES_reset",0x0,False,TCDS_PRBS_module_offset)

        # Check TXresetDONE = 1
        check_TXRESET_DONE = 0
        nb_retry = 0
        while (check_TXRESET_DONE == 0) and nb_retry <= self.MAX_ALLOW_RETRY:
            check_TXRESET_DONE = self.read64("TCDS_SERDES_reset",TCDS_PRBS_module_offset) & 0x40000000
            nb_retry +=1

        if nb_retry == self.MAX_ALLOW_RETRY:
            print("Something should be checked, becasue the previous access was unsuccessful !!  ")


     ##################################################################
    #Reset the PRBS counters
    def Reset_PRBS(self,TCDS_PRBS_module_offset):

        #Reset PRBS counter
        Reset_val = self.read64("TCDS_SERDES_PRBS_TEST",TCDS_PRBS_module_offset )
        Reset_PRBS = Reset_val | 0x10000000
        self.write64("TCDS_SERDES_PRBS_TEST",Reset_PRBS,False,TCDS_PRBS_module_offset )
        self.write64("TCDS_SERDES_PRBS_TEST",Reset_val,False,TCDS_PRBS_module_offset )

    ##################################################################
    def PRBS_locked(self,PRBS_module,DRP_ena_per_module,TCDS_PRBS_module_offset):

        #Check PRBS LOcked
        PRBS_locked = self.read64("TCDS_SERDES_PRBS_TEST",TCDS_PRBS_module_offset)
        PRBS_locked = (PRBS_locked   & 0xF000000) >> 24
        print("DAQ link : ",PRBS_module," locked if not nul (should be: ",hex(DRP_ena_per_module),") => ",hex(PRBS_locked & DRP_ena_per_module))


    ##################################################################
    def Read_PRBS_results_on_time(self,PRBS_module,DRP_ena_per_module,display,TCDS_PRBS_module_offset):

        result = [0] * 4
        #Reset PRBS err counter
        self.write64("TCDS_SERDES_ChDRP_cmd",0xF65E0000,False,TCDS_PRBS_module_offset)
        check = 0
        nb_retry = 0
        while (check != (DRP_ena_per_module) ) and nb_retry <= self.MAX_ALLOW_RETRY:
            check     = self.read64("TCDS_SERDES_ChDRP_st",TCDS_PRBS_module_offset) & 0xF
            #print(x," --  ",hex(check))
            nb_retry +=1

        if nb_retry == self.MAX_ALLOW_RETRY:
            print("Something should be checked, becasue the previous access was unsuccessful !!  ")

        Value_DRP     = self.read64("TCDS_SERDES_ChDRP_dt",TCDS_PRBS_module_offset)
        #print("Lower PRBS : " ,hex(Value_DRP))
        Link0_low    = (Value_DRP & 0xFFFF)
        Link1_low    = (Value_DRP & 0xFFFF0000) >> 16
        Link2_low    = (Value_DRP & 0xFFFF00000000) >> 32
        Link3_low    = (Value_DRP & 0xFFFF000000000000) >> 48

        self.write64("TCDS_SERDES_ChDRP_cmd",0xF65F0000,False,TCDS_PRBS_module_offset)
        check = 0
        nb_retry = 0
        while (check != (DRP_ena_per_module) ) and nb_retry <= self.MAX_ALLOW_RETRY:
            check     = self.read64("TCDS_SERDES_ChDRP_st",TCDS_PRBS_module_offset) & 0xF
            #print(x," --  ",hex(check))
            nb_retry +=1

        if nb_retry == self.MAX_ALLOW_RETRY:
            print("Something should be checked, becasue the previous access was unsuccessful !!  ")

        Value_DRP     = self.read64("TCDS_SERDES_ChDRP_dt",TCDS_PRBS_module_offset)
        #print("Higher PRBS : " ,hex(Value_DRP))
        Link0_high    = (Value_DRP & 0xFFFF)
        Link1_high    = (Value_DRP & 0xFFFF0000) >> 16
        Link2_high    = (Value_DRP & 0xFFFF00000000) >> 32
        Link3_high    = (Value_DRP & 0xFFFF000000000000) >> 48

        numb_link_in_quad = DRP_ena_per_module

        Link0        = Link0_high * 0x10000 + Link0_low
        Link1        = Link1_high * 0x10000 + Link1_low
        Link2        = Link2_high * 0x10000 + Link2_low
        Link3        = Link3_high * 0x10000 + Link3_low

        if display:
            numb_link_in_quad = DRP_ena_per_module
            print("*****************************")
            print("PRBS result DAQ : ",PRBS_module)
            print("Err link 0 : ", Link0)
            numb_link_in_quad = numb_link_in_quad >> 1
            if numb_link_in_quad != 0:
                print("Err link 1 : ", Link1)
            numb_link_in_quad = numb_link_in_quad >> 1
            if numb_link_in_quad != 0:
                print("Err link 2 : ", Link2)
            numb_link_in_quad = numb_link_in_quad >> 1
            if numb_link_in_quad != 0:
                print("Err link 3 : ", Link3)

        result[0] = Link0
        result[1] = Link1
        result[2] = Link2
        result[3] = Link3

        return result

    ##################################################################
    def PRBS_generate_error(self,TCDS_PRBS_module_offset):

        #Generate PRBS error  #(UG578 p155-p239)
        val = self.read64("TCDS_SERDES_PRBS_TEST",TCDS_PRBS_module_offset)
        Gen_error_PRBS = val | 0x80000000
        self.write64("TCDS_SERDES_PRBS_TEST",Gen_error_PRBS,False,TCDS_PRBS_module_offset)


    ##################################################################
    def Change_analog(self,TCDS_PRBS_module_offset):
        ###
        ###   A REVOIR
        ###
        # Actually all links are set identically

        #read analog parameters
##    val = self.read64("TCDS_SERDES_voltage",TCDS_PRBS_module_offset[x])
##    #print("analog values : " , hex(val))
##    TXdiff_rd         = (val & 0x1F         )
##    post_cursor_rd     = (val & 0x2E0         )>>5
##    pre_cursor_rd     = (val & 0x3C00     )>>10
##
##    TXdiff             = input2("Diff Swing value?  (dec)", TXdiff_rd)
##    post_cursor        = input2("PostCursor?        (dec)", post_cursor_rd)
##    pre_cursor        = input2("PreCursor?         (dec)", pre_cursor_rd)
##
##    #value = hex(rx_polarity)
##    interm    = (pre_cursor * 0x400) | (post_cursor * 0x20) | TXdiff
##    value = (interm * 0x1000000000000) |(interm * 0x100000000) | (interm * 0x10000) | (interm * 0x1)
##    print ("value written : ", hex(value))
##
##    for x in range(0,number_module):
##        print("Set Analog parameters for module : ",PRBS_module[x])
##        self.write64("TCDS_SERDES_voltage",value,False,TCDS_PRBS_module_offset[x])
        return

