#!/usr/bin/python
import sys
import os
import select
from I2CInterface import I2CInterface 
#
# Wwait until a key on the keyboard is pressed
#
def isData():
    return select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], [])


#
# Prompt and read input and optionally use default value
#
def input2(text, default=None):
    text_conc = text +" "+ str(default)
    input_val =  input(text_conc) or  default
    return int(input_val)

#
# Read the serial number from the DTH
# This number is the EUI-48 Node Address of the EEPROM chip which is used
# to hold the MAC addresses of the QSFPs.
#

def readSN( dth ) :

    i2c_SN = I2CInterface(dth, 0xA0, offsetWidth = 0x1, dataWidth = 0x1, debugFlag=0,
                          items={'i2c_a': 'sn_i2c_access_a', 'i2c_b': 'sn_i2c_access_b',
                                 'i2c_poll': 'sn_i2c_access_a_access_done'})

    # Read the EUI-48 Node Address WHICH IS 48
    sn = 0
    try:
        for i in range(0,6):
            value_read = i2c_SN.read(0xFA+i,noStop=True)
            sn += (value_read << (5-i)*8)
            
    except:
        print( "Could not read the serial number of the card" )
        sn = input( "Enter a serial number by hand. Only use hexadecimal characters and '-' : " )
    return sn
