#!/usr/bin/python
import sys
import os
import select
import time
import hal
import json
from I2CInterface import I2CInterface
from mylib.QSFP_PRBS_lib import QSFP_PRBS
from mylib.QSFP_ctrl_lib import QSFP_Ctrl
from mylib.Utility_lib import *

os.system('clear')

##################################################################

def ENA_QSFP():

    #   remove local reset
    print("Release Reset")
    dth.write("reset_hw_comp",0xF0000000)
    time.sleep(0.1)
    dth.write("reset_hw_comp",0x00000000)

    my_QSFP_control.Enable_QSFP()


##################################################################

def Setup_PRBS():

    # read if QSFP is present?
    QSFP_present =  dth.read64("qsfp_monitoring",0)
    print("QSFP present ? Each QSFP is a byte (bit 2 = 0?): ",hex(QSFP_present))
    for x in range(0,5):
        if (QSFP_present & 0x4 == 0):
            my_QSFP_PRBS_control.QSFP_PRBS_on(QSFP_offset[x])

        QSFP_present = QSFP_present >> 8

##################################################################

def Reset_PRBS():

    # read if QSFP is present?
    QSFP_present =  dth.read64("qsfp_monitoring",0)

    for x in range(0,5):
        if (QSFP_present & 0x4 == 0):
            my_QSFP_PRBS_control.Reset_PRBS(QSFP_offset[x])

        QSFP_present = QSFP_present >> 8

##################################################################
def PRBS_locked():

    # read if QSFP is present?
    QSFP_present =  dth.read64("qsfp_monitoring",0)

    for x in range(0,5):
        if (QSFP_present & 0x4 == 0):
            my_QSFP_PRBS_control.PRBS_locked(x,QSFP_offset[x])

        QSFP_present = QSFP_present >> 8

    done = input("return")
    return

##################################################################
def Read_results():

    Time_base = time.time()

    result = 0

    while 1:
        os.system ('clear')


        # read if QSFP is present?
        QSFP_present =  dth.read64("qsfp_monitoring",0)

        for x in range(0,5):
            if (QSFP_present & 0x4 == 0):
                result = my_QSFP_PRBS_control.Read_PRBS_results_on_time(x,True,QSFP_offset[x])

        QSFP_present = QSFP_present >> 8

        seconds2 = time.time()
        diff_sec = seconds2-Time_base
        berr = "{:.2e}".format(1/(diff_sec * 25000000000))

        print ("Diff time : ",berr )
        time.sleep(1)

        if isData():
                break

    done = input("return")
    return

##################################################################
def gen_error():

    # read if QSFP is present?
    QSFP_present =  dth.read64("qsfp_monitoring",0)

    for x in range(0,5):
        if (QSFP_present & 0x4 == 0):
            my_QSFP_PRBS_control.PRBS_generate_error(QSFP_offset[x])

        QSFP_present = QSFP_present >> 8

##################################################################
def Remove_PRBS():

    # read if QSFP is present?
    QSFP_present =  dth.read64("qsfp_monitoring",0)

    for x in range(0,5):
        if (QSFP_present & 0x4 == 0):
            my_QSFP_PRBS_control.QSFP_PRBS_off(QSFP_offset[x])

        QSFP_present = QSFP_present >> 8

##################################################################
def QSFP_power():

    my_QSFP_control.RxPower_QSFP()


    done = input("return")
    return

##################################################################
def QSFP_temp():

    my_QSFP_control.Temp_QSFP()


    done = input("return")
    return



