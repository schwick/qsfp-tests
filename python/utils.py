from glob import glob
from pprint import pprint
from copy import deepcopy
import re

def plot_div( fd, infile, prefix, imgtype ):
    fd.write( '<div id="'+prefix+'" class="plotdiv">' )
    fd.write( '<div>')
    fd.write( '<input name="' +prefix + '" class="fullwidth" type="range" min="0" max="31" oninput="changePlot()"/>\n')
    fd.write( '</div>')
    fd.write( '<h1>' + prefix + "  " +infile[:-4]+'</h1>')    
    for i in range(0,MAXPLOTS):
        fd.write('<div class="aframe" id="f_' + prefix + str(i) + '" >\n')
        fd.write('<h2> V_diff index: '+str(i)+'</h2>')
        fd.write( '<image src="' + prefix + str(i)+'.'+imgtype+'">\n')
        fd.write('</div>\n')
    fd.write('</div>\n')


class ScanResults:

    def __init__(self):
        self._dirs = glob( "scan_*/plots_berr*" )
        self._slots = []
        self._cards = []
        self._vendors = []
        self._qsfps = []
        self._resh = {}
        self._scanResultDirectories();

    @property
    def slots(self):
        return self._slots
    
    @property
    def vendors(self):
        return self._vendors
    
    @property
    def qsfps(self):
        return self._qsfps
    
    @property
    def cards(self):
        return self._cards
    
    @property
    def results(self):
        return self._resh
    
    def fillna( self ):
        r = deepcopy(self._resh)

        for slot in self._slots:
            for line in range(0,4):
                if line not in r[slot]:
                    r[slot][line] = {}
                for qsfp in self._qsfps:
                    if qsfp not in r[slot][line]:
                        r[slot][line][qsfp] = {}
                    for card in self._cards:
                        if card not in r[slot][line][qsfp]:
                            r[slot][line][qsfp][card] = "n.a"
        return r
    
    def _scanResultDirectories(self):
        
        for d in self._dirs:
            print("====" + dir)
            mo = re.match( r'scan_\d+/plots_berr_([^_]+)_(.+)_(\d)_([0-9a-f\-]+)$', d )
            if not mo:
                print( "no match: ", d)
                sys.exit()
            vendor = mo.group(1)
            qsfp = vendor + "_" + mo.group(2)
            slot = int(mo.group(3))
            card = mo.group(4)
        
            if slot not in self._resh:
                self._resh[slot] = { 0 : {},
                                     1 : {},
                                     2 : {},
                                     3 : {} }    
        
            if qsfp not in self._resh[slot][0]:
                self._resh[slot][0][qsfp] = {}
            if qsfp not in self._resh[slot][1]:
                self._resh[slot][1][qsfp] = {}
            if qsfp not in self._resh[slot][2]:
                self._resh[slot][2][qsfp] = {}
            if qsfp not in self._resh[slot][3]:
                self._resh[slot][3][qsfp] = {}
        
            self._resh[slot][0][qsfp][card] = d
            self._resh[slot][1][qsfp][card] = d
            self._resh[slot][2][qsfp][card] = d
            self._resh[slot][3][qsfp][card] = d
            
            print( mo.group(0))
            print (slot, vendor, qsfp, card)
            
            if vendor not in self._vendors:
                self._vendors.append(vendor)
            if qsfp not in self._qsfps:
                self._qsfps.append(qsfp)
            if slot not in self._slots:
                self._slots.append( slot )
            if card not in self._cards:
                self._cards.append( card )

            # Not all qsfps have been tested in all slots of all card. 
            
            self._slots.sort()
            self._vendors.sort()
            self._qsfps.sort()
            self._cards.sort()
            print( "slots  : ",  self._slots)
            print( "vendors : ", self._vendors)
            print( "qsfps   : ", self._qsfps)
            print( "cards   : ", self._cards)

