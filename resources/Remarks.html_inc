
<h2>Tested DAQ Unit</h2>

The board tested was a %boardtype%.

The DAQUnit tested was number %daqunitno%  with serial number : %daqunit%.

<h2>Description of the test</h2>

All tests have been performed with PRBS-31 loopback (optical loopback with an
external fibre connected to the QSFPs. Every QSFP has the TX connected via
and external fibre to its own RX).

In total slots 0,1,2,3,4 have been systematically tested with 5
QSFPS (mono mode): 2 from from Juniper.

In this board one line on one QSFP has a missing back-drill which is the
reason why this line did not work correctly.

Each plot scans the parmeters precursor and postcursor (which set the
pre-emphasis) and the differential voltage. The two dimensional plots
are generated in the plane of hte pre-emphasis parameters whereas the
31 differential voltages can be made visible via the slider on top of
each plot (i.e. there is one 2 dimensional plot of the pre-emphasis
parameters for each differential voltage.)

The plots show the bit error rate in a color coding (heat-map).

<h3>Details on the plots important for interpretation</h3>
In each QSFP the errors for each of the four 25Gbps lines are recorded independently.
The following procedure has been applied for each measurement.

<ul>
  <li>The parameters (post- pre-cursor and diff voltage) are set for each measuremnt
    and after 10ms waiting time it is checked if all 4 25Gbps lines have locked in.
    If all 4 lines are NOT locked the test ends immediately and the next parameter configuration
    is tested.
  </li>
  <li>A loop is entered where first the test runs for 0.2 seconds. Then the
    encountered bit errors are read out for each line and the bit error rate
    is calculated for each line (using the total number of encounted erros
    divided by the total duration of the test.) If no error has been encountered
    the number of errors is set to '1' to have a upper limit of the bus error
    rate.
  </li>
  <li> The test is stopped if either all 4 lines have a bit error rate worse than
    0.01 or if the total test duration exceeds 2 seconds.
  </li>
</ul>  


It is not possible from the plot to distinguish if one error has
been really observed or if 0 errors have occured. 


<h3>Using the result page</h3>

<p>
The shown measurements have 4 columns each representing one of the four 25Gbps
lines in the QSFPs.
</p>
<p>
The 5 rows of plots correspond to the 5 different QSFPs in the DAQ Unit.
</p>

