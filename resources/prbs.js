function selectPlot()
{
    slot = $('input[name="slotchooser"]:checked').val();
    line = $('input[name="linechooser"]:checked').val();
    console.log( slot, line);
    if (slot != undefined && line != undefined ) 
	window.location = "slot_"+slot+"_line_"+line+".html";
}


function initPlot()
{
    $('div [id$=_16]').show();
    $('div#f_line_0_out_16').show();
    $('div#f_line_1_out_16').show();
    $('div#f_line_2_out_16').show();
    $('div#f_line_3_out_16').show();

    lp = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
    console.log(lp);
    slot = lp[5];
    line = lp[12];
    console.log(slot, line);
    $('input[name="slotchooser"][value="'+slot+'"]').attr('checked', true);
    $('input[name="linechooser"][value="'+line+'"]').attr('checked', true);
}

function globalChange( e )
{
    val = event.target.value;
    console.log("in gl", val );
    $('input.fullwidth').val(val);
    $('.aframe').hide();
    $('[id^=f_][id$=_'+val+']').show();

}

function changeLinePlot(e)
{
    val = event.target.value;
    console.log(val)
    prefix = event.target.getAttribute('name');
    idp = "f_".concat(prefix);
    idps = idp.concat( '_', val);
    console.log(idp, idps);
    $("[id^='" + idp + "']").hide();
    $("#" + idps).show();
    console.log(event);
}

function changePlot(e)
{
    val = event.target.value;
    prefix = event.target.getAttribute('name');
    idp = "f_" + prefix
    console.log(idp, prefix+val);
    $("[id^='" + idp + "']").hide();
    $("#f_"+prefix + val).show();
}
